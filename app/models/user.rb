require 'thm_search'

class User < ApplicationRecord
  attr_accessor :password
  alias_attribute :role, :user_class

  fuzzily_searchable :display_name

  has_many :tasks
  has_many :accesses
  has_many :feedbacks, foreign_key: "editor_id", dependent: :destroy

  before_create :set_default_user_class!

  def set_default_user_class!
    self.user_class ||= 'S'
  end

  if Rails.env.test?
    after_initialize :foo

    def foo
      self.username ||= Faker::Name.name
      self.display_name ||= Faker::Name.name
      self.user_class ||= 'S'
    end

    def valid_password?(password)
      if Rails.env == 'development'
        return true
      end

      return super(password)
    end

    #Include default devise modules. Others available are:
    #:confirmable, :lockable, :timeoutable and :omniauthable
    devise :database_authenticatable, :registerable, :trackable, :validatable#, :timeoutable
  elsif Rails.env.development?
    devise :database_authenticatable, :registerable, :trackable, :validatable

    def valid_password?(password)
      true
    end
  else # production || server_development
    devise :cas_authenticatable, :trackable, :timeoutable
  end

  def cas_extra_attributes=(extra_attributes)
    extra_attributes.each do |name, value|
      Rails.logger.info "cas Extras: #{name}: #{value}"
      case name.to_sym
        when :mail
          self.email = value
        when :displayName
          self.display_name = value
        when :userClass
          self.user_class = self.user_class || value
      end
    end

    amend_user_info!
  end

  def amend_user_info!
    return unless self.email.blank? || self.read_attribute(:display_name).blank?

    details = THMSearch::get_user_details(self.username)

    self.email = details[:email]
    self.display_name = details[:display_name]
  end

  def display_name
    name = self.read_attribute(:display_name)
    if name.blank?
      self.username
    else
      name
    end
  end

  def has_locked_task?(task_id)
    TaskLock.where(user_id: self.id, task_id: task_id).any?
  end
end
