class Submission < ApplicationRecord
  mount_uploader :submission, SubmissionUploader
  include AcceptCarrierwaveStringFiles

  accept_string_files :output

  belongs_to :task, class_name: 'Group' # Polymorphic und STI gehen nicht also Lade die Basisklasse, und rails erledigt den rest....
  belongs_to :user
  has_many :test_results, dependent: :destroy

  validates_presence_of :submission, if: Proc.new { |s| s.task.class == SubmissionTask or s.task.needs_source }
  validates_presence_of :task

  validates_length_of :output_content, :minimum => 1, if: Proc.new { |s| s.task.class == InputOutputTask }

  validate :check_submission_structure
  after_save :schedule_test

  def display_name
    return self.user.display_name unless self.user.nil?
    return self.name
  end

  def percent_rank
    all_submissions = self.task.submissions
    correct_for_submissions = Hash[all_submissions.map {|s| [s.id, 0] }]
    correct_for_submissions.merge! TestResult.correct.where(submission: all_submissions).group(:submission_id).count
    better_than_num = 0
    own_correct_count = correct_for_submissions[self.id]

    correct_for_submissions.values.each do |corrent_count|
      better_than_num += 1 if corrent_count <= own_correct_count #Also Counting submissions with same result as worse so 100% becomes possible
    end

    return better_than_num * 100 / all_submissions.count.to_f
  end

  def check_submission_structure
    return if self.errors.any?
    return if self.submission.file.nil?

    error = nil

    begin
      runner = self.task.test_runner
      validation_result = runner.validate_submission_structure self.submission, self.task
      puts "valid result:", validation_result.inspect
      error, details = validation_result[:error] unless validation_result.nil?
    rescue => e
      logger.warn "#{e}\n#{e.backtrace.join("\n")}"
      error = 'Datei ist Fehlerhaft! Struktur Validierung wurde mit Fehler beendet!'
    end

    self.warning_text = validation_result[:warning] unless validation_result.nil?
    self.errors.add(:submission, error) unless error.nil?
    self.errors.add(:details, details) unless details.nil?
  end

  def schedule_test
    self.task.test_suits.each do |test_suit|
      self.test_results.where(test_case: test_suit.test_cases.ids).destroy_all
      RunSubmissionTestJob.perform_later self.id, test_suit.id
    end

    self.task.moss_run.schedule_if_necessary if self.task.respond_to? :moss_run
  end
end
