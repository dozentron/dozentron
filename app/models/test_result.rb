class TestResult < ApplicationRecord
  belongs_to :test_case
  has_one :task, through: :test_case

  belongs_to :submission

  scope :correct, -> () { where successful: true }
  scope :wrong, -> () { where successful: [false, nil] }
end
