class Feedback < ApplicationRecord
  enum markup: [:markdown, :asciidoc]

  String :feedback

  belongs_to :task
  belongs_to :student, :class_name => 'User'
  belongs_to :editor, :class_name => 'User'

  # validates :user, uniqueness: {scope: :task}
end
