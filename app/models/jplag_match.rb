class JplagMatch < ApplicationRecord
  mount_uploader :left_html, JplagMatchHtmlUploader
  mount_uploader :right_html, JplagMatchHtmlUploader

  belongs_to :jplag_group
  belongs_to :submission
end
