require 'active_support/concern'

module AcceptCarrierwaveStringFiles
  extend ActiveSupport::Concern

  def self.included(base)
    base.extend ClassMethods
  end

  module ClassMethods
    def accept_string_files(*fields)
      fields.each do |field|
        self.mount_uploader field, LongTextUploader

        define_method "#{field}_content", -> () {
          content = self.send(field).read
          content.force_encoding('UTF-8') unless content.nil?
        }

        define_method "#{field}_content=" , -> (content) {
          text_io = StringIO.new content
          text_io.original_filename = 'Text'
          self.send("#{field}=".to_sym, text_io)

          if self.respond_to?("#{field}_length=".to_sym)
            length = (content.nil?) ? 0 : content.length
            self.send("#{field}_length=".to_sym, length)
          end
        }
      end
    end
  end
end