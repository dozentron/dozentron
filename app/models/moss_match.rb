class MossMatch < ApplicationRecord
  mount_uploader :submission_1_html, MossMatchHtmlUploader
  mount_uploader :submission_2_html, MossMatchHtmlUploader

  has_one :task, through: :moss_run

  belongs_to :submission_1, class_name: 'Submission', foreign_key: 'submission_1_id'
  belongs_to :submission_2, class_name: 'Submission', foreign_key: 'submission_2_id'

  belongs_to :moss_run
end
