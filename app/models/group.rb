class Group < ApplicationRecord
  include AcceptCarrierwaveStringFiles
  enum markup: [:markdown, :asciidoc]

  accept_string_files :text

  fuzzily_searchable :title
  has_ancestry orphan_strategy: :destroy

  belongs_to :user
  has_many :accesses, dependent: :destroy
  has_many :attachments, dependent: :destroy

  default_scope { order(title: :asc) }

  scope :task, -> { where(type: %w(Task SubmissionTask InputOutputTask)) }
  scope :subtrees,  -> (root_ids) do
    if root_ids.nil? || root_ids.empty?
      return Group.none
    end

    first_id = root_ids.shift
    query = where('ancestry LIKE ?', "#{first_id}/%").or(where(ancestry: first_id)).or(where(id: first_id))
    root_ids.each do |current_id|
      query = query.or(where('ancestry LIKE ?', "#{current_id}/%").or(where(ancestry: current_id)).or(where(id: current_id)))
    end

    query
  end

  validates_length_of :title, :minimum => 1, maximum: 100
  validate :unique_amongst_siblings   # This is important for the statistics because they use the titles as keys in hashes

  after_initialize :set_visible_default

  def unique_amongst_siblings
    if self.parent.nil?
      query = Group.roots
    else
      query = self.parent.children
    end

    query = query.where.not(id: self.id)
    titles = query.pluck :title

    errors.add(:title, 'Titel Existiert schon in dieser Gruppe!') if titles.include? self.title
  end

  def set_visible_default
    self.visible = true if self.visible.nil?
  end

  def submitters
    User.where id: submissions.pluck(:user_id)
  end

  def submissions
    task_ids = self.descendants.task.pluck :id

    Submission.where task_id: task_ids
  end

  def has_submissions?(ability)
    tasks = self.descendants.task
    Submission.accessible_by(ability).where(task_id: tasks.ids).any?
  end

  def average_result(ability, user=nil)
    query = self.descendants.task
    count = query.count
    return if count == 0
    result = 0

    query.each do |task|
      tmp = task.average_result(ability, user)
      result += tmp unless tmp.nil?
    end

    return result.to_f / count
  end

  def correct_wrong_data(ability)
    result = {}
    self.descendants.task.each do |task|
      result.merge!(task.correct_wrong_data(ability)) { |key, old, new| old + new }
    end

    return result
  end

  def exception_count_data(ability)
    result = {}

    self.descendants.task.each do |task|
      result.merge!(task.exception_count_data(ability)) { |key, old, new| old + new }
    end

    return result
  end

  def allow_subgroup?
    true
  end

  def allow_subtask?
    true
  end

  def has_moss_results?
    false
  end
end
