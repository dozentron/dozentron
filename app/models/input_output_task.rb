# coding: utf-8
class InputOutputTask < Task
  alias_attribute :input_generator, :stub

  has_many :inputs, dependent: :destroy

  validates_presence_of :input_generator


  def has_donwloadable_tests?
    false
  end


  def test_runner
    submission_tester_class = InputOutputTesterLanguages.runner_for_language(self.language)
    raise "No TestRunner available for InputOutputTest in #{self.language}" unless submission_tester_class

    return submission_tester_class.new
  end

  def input(user)
    input = self.inputs.find_by user: user
    input = generate_input(user) if input.nil?

    return input
  end

  private
  def generate_input(user)
    input_content = test_runner.generate_input self.input_generator

    input = self.inputs.build
    input.user = user
    input.input_content = input_content
    input.save!

    return input
  end

  def check_test_structure
    super

    begin
      error, details = self.test_runner.validate_input_generator_structure self.available_test, self.input_generator
    rescue => e
      logger.warn "#{e}\n#{e.backtrace.join("\n")}"
      error = 'Test ist Fehlerhaft! Überprüfung der Teststruktur wurde mit Fehler beendet!'
    end

    self.errors.add(:input_generator, error) unless error.nil?
    self.errors.add(:details, details) unless details.nil?
  end
end
