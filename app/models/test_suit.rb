class TestSuit < ApplicationRecord
  belongs_to :task, class_name: 'Group' # Polymorphic und STI gehen nicht also Lade die Basisklasse, und rails erledigt den rest....

  has_many :test_cases, dependent: :destroy
end
