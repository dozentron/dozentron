class Input < ApplicationRecord
  include AcceptCarrierwaveStringFiles
  accept_string_files :input

  belongs_to :user
  belongs_to :input_output_task
end
