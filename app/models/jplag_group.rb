class JplagGroup < ApplicationRecord
  belongs_to :submission
  belongs_to :jplag_run

  has_many :jplag_matches, dependent: :destroy
end
