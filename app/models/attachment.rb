class Attachment < ApplicationRecord
  mount_uploader :file, AttachmentUploader
  belongs_to :group
  validates_presence_of :file
end
