class MossRun < ApplicationRecord
  belongs_to :task, class_name: 'Group'

  has_many :moss_matches, dependent: :destroy

  def schedule_if_necessary
    return unless self.task.run_moss?
    return if self.scheduled || self.running
    return unless self.last_run.nil? || (self.last_run + 1.day) < Time.now # atleast one day passed
    return unless self.task.submissions.count > 10

    RunMossJob.set(wait: 2.hours).perform_later self.id
    self.update_attributes scheduled: true, last_run: Time.now
  end

  def run
    return if self.running
    return unless self.last_run.nil? || (self.last_run + 20.seconds) < Time.now

    RunMossJob.perform_later self.id
    self.update_attributes scheduled: true, last_run: Time.now
  end

  def self.can_run_moss?
    !Rails.application.secrets.moss_user_id.nil?
  end
end
