# coding: utf-8
class Ability
  include CanCan::Ability

  def initialize(user)
    # Define abilities for the passed in user here. For example:
    #
    #   user ||= User.new # guest user (not logged in)
    #   if user.admin?
    #     can :manage, :all
    #   else
    #     can :read, :all
    #   end
    #
    # The first argument to `can` is the action you are giving the user
    # permission to do.
    # If you pass :manage it will apply to every action. Other common actions
    # here are :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on.
    # If you pass :all it will apply to every resource. Otherwise pass a Ruby
    # class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the
    # objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details:
    # https://github.com/CanCanCommunity/cancancan/wiki/Defining-Abilities

    if user.role == 'M' #TODO Zukünfig Dozent
      can :index, User
      can :manage, Group, user_id: user.id
      can :manage, Access, group: { user_id: user.id }
      can :manage, Attachment

      groups_ids_access = Group.subtrees(user.accesses.pluck :group_id).pluck :id
      #
      # can [:index, :show, :results, :new, :create, :edit, :update, :sub_group, :sub_task, :statistics], Group.subtrees(user.accesses.pluck :group_id) do |group|
      #   group.root.accesses.where(user: user).any?
      # end

      can :manage, MossRun, task: { user_id: user.id }
      can :manage, JplagRun, task: { user_id: user.id }

      can :manage, TestSuit, task: { user_id: user.id }
      can :manage, TestCase, task: { user_id: user.id }
      can :manage, TestResult, task: { user_id: user.id }
      can :manage, Submission, task: { user_id: user.id }
      can :manage, MossMatch, submission_1: { task: { user_id: user.id } }
      can :manage, JplagMatch

      #accesses
      can [:index, :show, :results, :new, :create, :edit, :update, :sub_group, :sub_task, :statistics, :moss, :jplag, :download, :download_tests], Group, id: groups_ids_access
      can :destroy, Task, parent_id: groups_ids_access

      can [:index, :run], MossRun, task: { id: groups_ids_access }

      can [:index], TestSuit, task: { id: groups_ids_access }
      can [:index], TestCase, task: { id: groups_ids_access }
      can [:index], TestResult, task: { id: groups_ids_access }
      can [:index, :download,:feedback], Submission, task: { id: groups_ids_access }
      can [:index, :run, :show], MossMatch, submission_1: { task: { id: groups_ids_access } }
      can :manage, Feedback
      can :manage, TaskLock
    elsif user.role == 'S' #TODO Zukünftig Student
      can :toggle, TaskLock, user_id: user.id
    end

    can :index, User, id: user.id
    can :index, TestSuit
    can :index, TestCase
    can :index, TestResult, submission: { user_id: user.id }
    can :show, Attachment
    can :show, Feedback, student: user

    can [:index, :show, :results, :download_tests], Group, visible: true

    can [:show], Input, user_id: user.id

    can [:index, :show, :new, :create, :edit, :update, :download], Submission, user_id: user.id
  end
end
