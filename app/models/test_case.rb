class TestCase < ApplicationRecord
  belongs_to :test_suit
  has_one :task, through: :test_suit
  has_many :test_results, dependent: :destroy
end
