# coding: utf-8
class Task < Group
  mount_uploader :test, SubmissionTestUploader
  mount_uploader :hidden_test, SubmissionTestUploader
  mount_uploader :stub, SubmissionTestUploader

  has_many :submissions, dependent: :destroy
  has_many :test_suits, dependent: :destroy
  has_many :test_cases, :through => :test_suits
  has_many :test_results, :through => :submissions
  has_many :feedbacks, dependent: :destroy

  belongs_to :user

  #check that AT LEAST ONE of [test,hidden_test] is available
  validates_presence_of :test, if: proc{|obj| obj.hidden_test.current_path.nil?}
  validates_presence_of :hidden_test, if: proc{|obj| obj.test.current_path.nil?}

  validates :parent_id, presence: true
  validate :check_test_structure

  after_initialize :set_deadline_default

  before_save :create_test_structure_records, if: Proc.new { |o| o.new_record? || o.test_changed? || o.hidden_test_changed? || o.stub_changed? }
  after_commit :revalidate_and_rerun_tests, on: [:update]

  def test_jar_list
    return [self.test,self.hidden_test].select { |uploader| not uploader.current_path.nil? }
  end

  #either :test or :hidden_test ; one of them is available because of the validation
  def available_test
    return (self.hidden_test.current_path.nil?) ? self.test : self.hidden_test
  end

  def has_donwloadable_tests?
    not self.test.current_path.nil?
  end

  def test_runner
    raise 'Not Implemented!'
  end

  def has_submissions?(ability)
    Submission.accessible_by(ability).where(task_id: self.id).any?
  end

  def correct_wrong_data(ability)
    correct = correct_count(ability)
    wrong = wrong_count(ability)

    return {
        'korrekt' => correct,
        'falsch' => wrong
    }
  end

  def exception_count_data(ability)
    self.test_results.accessible_by(ability, :index).where.not(failure_exception: nil).group(:failure_exception).count
  end

  def wrong_count_data(ability)
    self.test_results.wrong.accessible_by(ability, :index).joins(:test_case).group('"test_cases"."name"').count
  end

  def average_result(ability, user=nil)
    correct = correct_count ability, user

    count_query = self.test_results.accessible_by(ability, :index)
    count_query = count_query.where(submission: Submission.where(user: user)) unless user.nil?
    count = count_query.count
    return if count == 0

    return correct.to_f / count * 100
  end

  def allow_subgroup?
    false
  end

  def allow_subtask?
    false
  end

  private

  def correct_count(ability, user=nil)
    correct_query = self.test_results.correct.accessible_by(ability, :index)
    correct_query = correct_query.where(submission: Submission.where(user: user)) unless user.nil?

    return correct_query.count
  end

  def wrong_count(ability, user=nil)
    wrong_query = self.test_results.wrong.accessible_by(ability, :index)
    wrong_query = wrong_query.where(submission: Submission.where(user: user)) unless user.nil?

    return wrong_query.count
  end

  def create_test_structure_records
    test_runner = self.test_runner
    no_test_cases = true

    begin
      Task.transaction do
        self.test_suits.destroy_all

        test_suit_names = test_runner.test_suit_names_for(test_jar_list, self.stub)
        test_suit_names.each do |test_suit_name|
          test_cases = test_runner.test_cases_for_testsuit(test_jar_list, self.stub, test_suit_name)

          if test_cases.any?
            no_test_cases = false

            test_suit = TestSuit.new
            test_suit.name = test_suit_name
            self.test_suits << test_suit

            test_cases.each do |test_case_name, values|
              test_case = TestCase.new
              test_case.name = test_case_name
              test_case.points = values['points']
              # test_case.extras = values['extras'] # for future
              test_suit.test_cases << test_case
            end
          end
        end

        if no_test_cases
          raise 'Es konnten keine Testfälle gefunden werden!'
        end
      end
    rescue => e
      self.errors.add :test, e.to_s
      throw(:abort)
    end
  end

  def check_test_structure
    return if self.errors.any?

    error = nil
    details = nil

    begin
      validation_result = self.test_runner.validate_test_structure available_test, self.stub
      error, details = validation_result[:error] unless validation_result.nil?
    rescue => e
      logger.warn "#{e}\n#{e.backtrace.join("\n")}"
      error = 'Test ist Fehlerhaft! Überprüfung der Teststruktur wurde mit Fehler beendet!'
    end
    self.errors.add(:test, error) unless error.nil?
    self.errors.add(:details, details) unless details.nil?
  end

  def revalidate_and_rerun_tests
    RerunTestJob.perform_later self.id
  end

  def set_deadline_default
    self.deadline = Time.now + 7.days if self.deadline.nil?
  end
end
