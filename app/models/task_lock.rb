class TaskLock < ApplicationRecord
  validates_presence_of :user_id
  validates_presence_of :task_id

  belongs_to :task
  belongs_to :user
end
