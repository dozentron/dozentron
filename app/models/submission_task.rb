class SubmissionTask < Task
  has_one :moss_run, dependent: :destroy, foreign_key: 'task_id'
  has_one :jplag_run, dependent: :destroy, foreign_key: 'task_id'

  has_many :moss_matches, through: :moss_run
  has_many :jplag_groups, through: :jplag_run

  before_save :prepare_moss_run, if: :new_record?

  def test_runner
    submission_tester_class = SubmissionTesterLanguages.runner_for_language(self.language)
    raise "No TestRunner available for language: #{self.language}" unless submission_tester_class

    return submission_tester_class.new
  end

  # return language used for moss or nil if moss is not supported
  def moss_language
    runner = test_runner

    return nil unless runner.respond_to? :moss_language
    return runner.moss_language
  end

  def moss_matches
    submission_ids = self.submissions.ids
    MossMatch.where('submission_1_id IN (?) OR submission_2_id IN (?)', submission_ids, submission_ids)
  end

  def run_moss?
    self.run_moss
  end

  def has_moss_results?
    self.moss_run.moss_matches.any?
  end

  def has_jplag_results?
    self.jplag_run && self.jplag_run.jplag_groups.any?
  end

  private
  def prepare_moss_run
    self.build_moss_run
  end
end
