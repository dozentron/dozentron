class Access < ApplicationRecord
  belongs_to :user
  belongs_to :group

  validates :user, presence: true
  validates :group, presence: true

  validate :group_is_root

  def group_is_root
    return false unless self.group.nil?
    self.group.root?
  end
end
