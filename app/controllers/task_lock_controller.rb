class TaskLockController < ApplicationController
  def toggle
    @lock = TaskLock.find_by toggle_params

    if @lock.nil?
      @lock = TaskLock.new toggle_params
      authorize! :toggle, @lock

      @lock.save
      flash[:notice] = 'Abgabe wurde für die Korrektur freigegeben.'
      render json: { locked: true }
    else
      authorize! :toggle, @lock

      unless @lock.corrected?
        @lock.destroy!
        flash[:notice] = 'Bereitschaft zur Korrektur wurde zurückgezogen.'
        render json: { locked: false }
      else
        flash[:notice] = 'Ihre Abgabe wurde bereits korrigiert.'
        render json: { locked: true }
      end
    end
  end

  def toggle_corrected
    @lock = TaskLock.find_by toggle_params

    if @lock.nil?
      @lock = TaskLock.new toggle_params
      authorize! :toggle, @lock
      @lock.save
    end

    authorize! :correct, @lock

    @lock.update_attribute :corrected, !@lock.corrected?
    render json: { corrected: @lock.corrected? }
  end

  private
  def toggle_params
    params.permit(:task_id, :user_id)
  end
end
