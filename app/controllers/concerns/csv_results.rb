require 'active_support/concern'

module CSVResults
  extend ActiveSupport::Concern

  def csv_results_task(submissions, test_cases)
    CSV.generate(headers: true) do |csv|
      headers = ['Name', 'Kuerzel', 'E-Mail']
      headers = headers + test_cases.pluck(:name)
      csv << headers

      submissions.each do |submission|
        user = submission.user
        values = [submission.display_name]
        values.push (user.nil? ? nil : user.username)
        values.push (user.nil? ? nil : user.email)

        test_cases.each do |test_case|
          test_result = submission.test_results.where(test_case_id: test_case.id).first
          next values.push nil if test_result.nil?
          values.push (test_result.successful ? 1 : 0)
        end

        csv << values
      end
    end
  end

  def csv_results_groups(users, groups)
    CSV.generate(headers: true) do |csv|
      headers = ['Name', 'Kuerzel', 'E-Mail']
      headers = headers + groups.pluck(:title)
      csv << headers

      users.each do |user|
        values = [user.display_name, user.username]

        groups.each do |group|
          average = group.average_result(current_ability, user)
          next values.push nil if average.nil?
          values.push average
        end

        csv << values
      end
    end
  end

end