require 'active_support/concern'

module ZipSubmissions
  extend ActiveSupport::Concern

  def zip_submissions(submissions, path)
    Zip::File.open(path, Zip::File::CREATE) do |zip|
      submissions.each do |submission|
        unless submission.submission.current_path.nil?
          path = submission.submission.current_path
          name = submission.user.nil? ? '' : "#{submission.user.display_name.gsub(" ", "_")}-#{submission.user.username}-"
          zip.add "#{name}#{submission.id}-#{File.basename(path)}", path
        end

        # Skip current iteration if output_content is nil or empty
        next if submission.output_content.nil? || submission.output_content.empty?

        Tempfile.open(['output', '.txt'], encoding: 'utf-8') do |output_file|
          output_file.write submission.output_content
          zip.add "#{name}#{submission.id}-output.txt",output_file
          zip.add "#{name}#{submission.id}-input.txt",submission.task.input(submission.user).input.current_path
        end
      end
    end
  end
end
