class MossMatchesController < SecureController
  skip_load_and_authorize_resource only: :run_moss

  def show
    @group = @moss_match.task
    @submission_1_html = IO.read @moss_match.submission_1_html.current_path
    @submission_2_html = IO.read @moss_match.submission_2_html.current_path
  end

  def run_moss
    @task = Task.find_by! id: params[:submission_task_id]

    authorize! :run, @task.moss_run
    @task.moss_run.run
  end
end
