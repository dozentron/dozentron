class AccessesController < SecureController
  def index
    @group = Group.find_by id: params[:group_id]
    redirect_to group_accesses_path(@group.root) unless @group.root?

    @access = Access.new
    @access.group = @group

    authorize! :grant_access, @group
  end

  def create
    raise 'Not root group' unless @access.group.root?

    authorize! :grant_access, @access.group

    @saved = @access.save
  end

  def destroy
    authorize! :destroy, @access
    @access.destroy!
  end

  private
  def access_params
    params.require(:access).permit(:group_id, :user_id)
  end
end
