# coding: utf-8
class InputOutputTasksController < TasksController

  def show
    @group = @input_output_task
    @submission = @input_output_task.submissions.find_by(user: current_user) || @input_output_task.submissions.build
    @input = @input_output_task.input(current_user)

    unless @submission.new_record?
      @submission.valid?
    end
  end

  def new
    parent_id = params[:group_id]

    if parent_id.nil?
      flash[:error] = 'Aufgabe muss zu einer Gruppe gehören'
      redirect_back fallback_location: {controller: :groups, action: :index}
    else
      parent = Group.find_by! id: parent_id
      @input_output_task.parent = parent
      @group = parent
    end

    @test_file = @input_output_task.test.current_path unless @input_output_task.test.nil?
    @test_file = @input_output_task.test_cache.current_path unless @input_output_task.test_cache.nil?
    @test_file = File.basename @test_file unless @test_file.nil?

    @stub_file = @input_output_task.stub.current_path unless @input_output_task.stub.nil?
    @stub_file = @input_output_task.stub_cache.current_path unless @input_output_task.stub_cache.nil?
    @stub_file = File.basename @test_file unless @test_file.nil?
  end

  def create
    @input_output_task.user = @input_output_task.parent.user

    if @input_output_task.save
      flash[:notice] = 'Aufgabe erfolgreich erstellt'

      redirect_to @input_output_task
    else
      flash[:error] = 'Konnte Aufgabe nicht speichern'
      render :new
    end
  end

  def update
    old_generator = @input_output_task.input_generator
    super
    if old_generator != @input_output_task.input_generator
      @input_output_task.inputs.each(&:destroy)
    end
  end

  def set_task
    params[:task_id] = params[:input_output_task_id]
    @task = @input_output_task
    @group = @task
  end

  private
  def task_params
    input_output_task_params
  end

  def input_output_task_params
    params.require(:input_output_task).permit(:title, :text_content,:markup,  :deadline, :visible, :bonus, :parent_id, :test, :test_cache, :hidden_test,:hidden_test_cache,:input_generator, :stub_cache, :needs_source, :language)
  end
end
