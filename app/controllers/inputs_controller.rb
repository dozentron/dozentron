class InputsController < SecureController
  def show
    render file: @input.input.current_path, layout: false, content_type: 'text/plain'
  end
end
