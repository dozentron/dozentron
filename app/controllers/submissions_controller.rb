# coding: utf-8
require 'string_io'

class SubmissionsController < SecureController
  include ZipSubmissions
  skip_load_resource only: :batch_upload

  def index
  end

  def show
  end

  def new
    @task = Task.find_by! id: params[:task_id]
    @submission.task = @task
  end

  def create
    #TODO: merge create & update into 1 method ?
    @task = @submission.task
    @submission_task = @task
    @input_output_task = @task

    @submission.user = current_user
    @input = @input_output_task.try :input, current_user

    tmp = @task.submissions.find_by(user: current_user)
    unless tmp.nil?
      # User used browser back button and is about to create a second Submission so lets make sure he doesn't
      @submission = tmp
      @submission.assign_attributes submission_params
    end

    if @task.is_a?(JshellTask)
      #create the "submitted file" from users input
      Tempfile.open(["input", ".jshell"], encoding: 'utf-8') do |file|
        file.write(submission_params[:output_content])
        @submission.submission = file
      end
    end

    if not @task.deadline.nil? and @task.deadline.past?
      flash[:alert] = 'Abgabe nicht Erfolgreich. Deadline für diese Aufgabe ist abgelaufen.'
      render "#{@task.class.to_s.underscore}s/show"
    elsif @submission.save
      flash[:notice] = 'Abgabe erfolgreich.'
      redirect_to polymorphic_url([@task, :results])
    else
      flash[:alert] = 'Abgabe nicht Erfolgreich.'
      render "#{@task.class.to_s.underscore}s/show"
    end
  end

  def edit
  end

  def update
    @task = @submission.task
    @submission_task = @task
    @input_output_task = @task
    @input = @input_output_task.try :input, current_user

    if @task.is_a?(JshellTask)
      #create the "submitted file" from users input
      Tempfile.open(["input", ".jshell"], encoding: 'utf-8') do |file|
        file.write(submission_params[:output_content])
        @submission.submission = file
      end
    end

    if not @task.deadline.nil? and @task.deadline.past?
      flash[:alert] = 'Abgabe nicht Erfolgreich. Deadline für diese Aufgabe ist abgelaufen.'
      render "#{@task.class.to_s.underscore}s/show"
    elsif @submission.update_attributes submission_params
      flash[:notice] = 'Abgabe erfolgreich.'
      redirect_to polymorphic_url([@task, :results])
    else
      flash[:alert] = 'Abgabe nicht Erfolgreich.'
      render "#{@task.class.to_s.underscore}s/show"
    end
  end

  def download
    @submission = Submission.find_by! id: params[:submission_id]

    path = @submission.submission.current_path || @submission.output.path
    name = @submission.user.nil? ? '' : "#{@submission.user.username}-#{@submission.user.display_name.gsub(" ", "_")}-"

    authorize! :download, @submission
    if @submission.output_content.nil? || @submission.output_content.empty?
      send_file @submission.submission.current_path, filename: "#{name}#{File.basename(path)}"
    else
      Tempfile.open do |f|
        zip_submissions [@submission], f.path
        send_file f.path, filename: "#{name}#{File.basename(path, '.*')}.zip"
      end
    end

  end

  def destroy
  end

  def batch_upload
    submissions = []
    invalid_count = 0

    @task = Task.find_by! id: params[:task_id]
    @submission_task = @task
    @submission = Submission.new
    submission_batch = params[:submission_batch]

    authorize! :batch_upload, @task

    if (not submission_batch.nil?) && MimeMagic.by_magic(submission_batch) == "application/zip"
      submission_batch.rewind

      Tempfile.open(encoding: 'utf-8') do |file|
        IO.copy_stream submission_batch, file
        file.close
        Zip::File.open(file.path) do |zip|
          zip.entries.each do |entry|
            io = StringIO.new entry.get_input_stream.read
            io.original_filename = entry.name

            s = Submission.new
            s.name = entry.name
            s.task = @task
            s.submission = io

            submissions << s
          end
        end
      end

      submissions.each do |submission|
        invalid_count += 1 unless submission.valid?
        submission.save validate: false
      end

      flash[:notice] = "Abgabe erfolgreich. #{invalid_count} Abgaben haben Probleme"
      redirect_to polymorphic_url([@task, :results])
    else
      flash[:alert] = 'Abgabe nicht Erfolgreich.'
      render "#{@task.class.to_s.underscore}s/show"
    end
  end

  private
  def submission_params
    params.require(:submission).permit(:task_id, :submission, :submission_cache, :name, :output_content)
  end
end
