# coding: utf-8
class TasksController < SecureController
  include ZipSubmissions
  include CSVResults

  before_action :set_task
  before_action :set_file_name_attributes

  def edit
    @group = @task
  end

  def create
    @task.user = @task.parent.user

    if @task.save
      flash[:notice] = 'Aufgabe erfolgreich erstellt'

      redirect_to @task
    else
      flash[:error] = 'Konnte Aufgabe nicht speichern'
      @group = @task
      render :new
    end
  end

  def update
    if @task.update_attributes task_params
      flash[:notice] = 'Aufgabe erfolgreich aktualisiert'

      redirect_to @task
    else
      @group = @task
      render :edit
    end
  end

  def destroy
    authorize! :destroy, @task
    @parent = @task.parent
    @task.destroy

    flash[:notice] = "#{@task.title} erfolgreich gelöscht!"

    if @parent.nil?
      redirect_to root_path
    else
      redirect_to polymorphic_path @parent
    end
  end

  def results
    @task = Task.includes(:submissions, :test_cases, :test_results).accessible_by(current_ability, :index).find_by! id: params[:task_id]
    @group = @task
    @submissions = @task.submissions.accessible_by(current_ability, :index).includes(:user).order('users.display_name ASC')
    @submission = @submissions.first if @submissions.count == 1
    @test_cases = @task.test_cases.accessible_by(current_ability, :index)

    if cannot? :statistics, @task
      @test_results = @submission.test_results unless @submission.nil?
      @unfinished = @test_results.count < @test_cases.count unless @test_results.nil?
    end


    if cannot?(:statistics, @task) and @submission.nil?
      flash[:alert] = 'Sie müssen eine Abgabe machen, bevor sie Ergebnisse einsehen können!'

      redirect_to @task
    else
      respond_to do |format|
        format.html
        format.csv { send_data csv_results_task(@submissions, @test_cases), filename: "#{@task.title}_results.csv" }
      end
    end
  end

  def download_submissions
    @task = Task.find_by! id: params[:task_id]

    authorize! :download, @task

    Tempfile.open do |f|
      f.close
      submissions = @task.submissions

      if params[:not_corrected] == "true"
        submissions = @task.submissions.reject { |s| s.user.has_locked_task?(@task) }
      end

      zip_submissions submissions, f.path

      send_file f.path, filename: "#{@task.title[0..20]}.zip"
    end
  end

  def statistics
    @task = Task.find_by! id: params[:task_id]
    @group = @task
    authorize! :statistics, @task

    @submissions = @task.submissions
  end

  def set_task
    raise 'set_task of TasksController not implemented!'
  end

  def task_params
    raise 'I need an implementaion *read with creepy voice*'
  end

  def set_file_name_attributes
    return if @task.nil?

    @test_file = @task.test.current_path unless @task.test.nil?
    @test_file = @task.test_cache unless @task.test_cache.nil?
    @test_file = File.basename @test_file unless @test_file.nil?

    @hidden_test = @task.hidden_test.current_path unless @task.hidden_test.nil?
    @hidden_test = @task.hidden_test_cache unless @task.hidden_test_cache.nil?
    @hidden_test = File.basename @hidden_test unless @hidden_test.nil?

    @stub_file = @task.stub.current_path unless @task.stub.nil?
    @stub_file = @task.stub_cache unless @task.stub_cache.nil?
    @stub_file = File.basename @stub_file unless @stub_file.nil?
  end

end
