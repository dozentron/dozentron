# coding: utf-8
class GroupsController < SecureController
  include CSVResults

  skip_load_resource only: [:index]
  skip_load_and_authorize_resource only: :autocomplete_title

  def index
    @groups = Group.accessible_by(current_ability, :index)

    unless params[:title].nil?
      @groups = @groups.find_by_fuzzy_title(params[:title], limit: 10)
    else
      @groups = @groups.roots
    end
  end

  def show
  end

  def new
    parent_id = params[:group_id]

    unless parent_id.nil?
      @parent = Group.find_by! id: parent_id
      @group.parent = @parent
    end
  end

  def create
    @group.user = (@group.parent.nil?) ? current_user : @group.parent.user

    if @group.save
      flash[:notice] = 'Gruppe erfolgreich erstellt'

      parent = @group.parent

      unless parent.nil?
        redirect_to parent
      else
        redirect_to action: :index
      end
    else
      render :new
    end
  end

  def edit
  end

  def update
    @group.update_attributes group_params

    if @group.save
      flash[:notice] = 'Gruppe erfolgreich aktualisiert'

      redirect_to @group
    else
      render :edit
    end
  end

  def destroy
    authorize! :destroy, @group
    @parent = @group.parent
    @group.destroy

    flash[:notice] = "#{@group.title} und Kinder erfolgreich gelöscht!"

    if @parent.nil?
      redirect_to root_path
    else
      redirect_to polymorphic_path @parent
    end
  end

  def results
    @group = Group.find_by id: params[:group_id]

    authorize! :results, @group

    @children = @group.children.accessible_by(current_ability)
    @submitters = @group.submitters.accessible_by(current_ability)

    respond_to do |format|
      format.html
      format.csv { send_data csv_results_groups(@submitters, @children), filename: "#{@group.title}_results.csv" }
    end
  end

  def statistics
    @group = Group.find_by! id: params[:group_id]
    authorize! :statistics, @group

    @submissions = @group.submissions
  end

  def autocomplete_title
    term = params[:term]
    groups = Group.find_by_fuzzy_title(term, limit: 10)

    render json: groups.map { |group| { id: group.id, value: group.title} }
  end

  private

  def group_params
    params.require(:group).permit(:title, :text_content, :visible, :parent_id,:markup)
  end
end
