# coding: utf-8
class JshellTasksController < TasksController
	def new
		#copied from SubmissionTasksController
		parent_id = params[:group_id]

		if parent_id.nil?
			flash[:error] = 'Aufgabe muss zu einer Gruppe gehören'
			redirect_back fallback_location: {controller: :groups, action: :index}
		else
			parent = Group.find_by! id: parent_id
			@jshell_task.parent = parent
			@group = parent
		end
	end

	def show
		#copied from SubmissionTasksController
		@submission = @jshell_task.submissions.find_by(user: current_user) || @jshell_task.submissions.build

		unless @submission.new_record?
			@submission.valid?
		end
	end

	def create
		@jshell_task.user = @jshell_task.parent.user

		if @jshell_task.save
			flash[:notice] = 'Aufgabe erfolgreich erstellt'

			redirect_to @jshell_task
		else
			flash[:error] = 'Konnte Aufgabe nicht speichern'
			render :new
		end
	end

	def set_task
    params[:task_id] = params[:jshell_task_id]
    @task = @jshell_task
    @group = @task
  end

	private
	def task_params
		jshell_task_params
	end

	def jshell_task_params
		params.require(:jshell_task).permit(
			:title,
			:text_content,
      :markup,
			:deadline,
			:visible,
			:parent_id,
			:test,
			:test_cache,
			:hidden_test,
			:hidden_test_cache,
			:language)
	end

end
