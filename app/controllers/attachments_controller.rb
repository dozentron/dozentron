class AttachmentsController < SecureController

  def create
    @attachment.save!
    hash = {
      name: File.basename(@attachment.file.current_path),
      url: attachment_url(@attachment),
      isImage: MimeMagic.by_path(@attachment.file.current_path).image?
    }
    render json: hash
  end

  def show
    send_file @attachment.file.current_path
  end

  private
  def attachment_params
    params.require(:attachment).permit(:file)
  end
end
