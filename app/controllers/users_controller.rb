class UsersController < ApplicationController
  def autocomplete_display_name
    term = params[:term]
    users = User.find_by_fuzzy_display_name(term, limit: 10)

    render json: users.select{|user| user.role == 'M'}.map { |users| { id: users.id, value: users.display_name} }
  end
end
