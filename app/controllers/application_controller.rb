class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :null_session

  before_action :prepare_exception_notifier

  layout false

  rescue_from CanCan::AccessDenied do |exception|
    redirect_to root_url, :alert => exception.message
  end

  def index
    if user_signed_in?
      redirect_to groups_path
    end
  end

  private
  def prepare_exception_notifier
    request.env["exception_notifier.exception_data"] = {
        :current_user => current_user
    }
  end
end
