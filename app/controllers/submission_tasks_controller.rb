# coding: utf-8
class SubmissionTasksController < TasksController
  skip_authorize_resource only: [:moss_results, :moss_graph, :jplag_results]

  def show
    @submission = @submission_task.submissions.find_by(user: current_user) || @submission_task.submissions.build
    unless @submission.new_record?
      @submission.valid?
    end
  end

  def new
    parent_id = params[:group_id]

    if parent_id.nil?
      flash[:error] = 'Aufgabe muss zu einer Gruppe gehören'
      redirect_back fallback_location: {controller: :groups, action: :index}
    else
      parent = Group.find_by! id: parent_id
      @submission_task.parent = parent
      @group = parent
    end
  end

  def download_tests
    @submission_task = SubmissionTask.find_by! id: params[:submission_task_id]
    authorize! :download_tests, @submission_task
    send_file @submission_task.test.current_path
  end

  def moss_results
    @submission_task = Task.includes(:moss_matches).accessible_by(current_ability, :index).find_by! id: params[:submission_task_id]
    @group = @submission_task
    @moss_matches = @submission_task.moss_matches.order('greatest(moss_matches.match_percentage_1, moss_matches.match_percentage_2) DESC').accessible_by(current_ability, :index)

    authorize! :moss, @submission_task
  end

  def jplag_results
    @submission_task = Task.includes(:moss_matches).accessible_by(current_ability, :index).find_by! id: params[:submission_task_id]
    @group = @submission_task
    @jplag_groups = @submission_task.jplag_groups


    authorize! :jplag, @submission_task
  end

  def moss_graph
    @submission_task = Task.includes(:submissions, :moss_matches).accessible_by(current_ability, :index).find_by! id: params[:submission_task_id]
    @group = @submission_task
    @submissions = @submission_task.submissions.accessible_by(current_ability, :index)
    @moss_matches = @submission_task.moss_matches.accessible_by(current_ability, :index)

    authorize! :moss, @submission_task
  end


  def set_task
    params[:task_id] = params[:submission_task_id]
    @task = @submission_task
    @group = @task
  end

  private
  def task_params
    submission_task_params
  end

  def submission_task_params
    params.require(:submission_task).permit(:title, :text_content, :markup, :deadline, :visible, :bonus, :parent_id, :test, :test_cache, :hidden_test,:hidden_test_cache,:stub, :stub_cache, :language, :run_moss)
  end
end
