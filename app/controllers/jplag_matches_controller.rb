class JplagMatchesController < SecureController
  def show
    @left_html = IO.read @jplag_match.right_html.current_path
    @right_html = IO.read @jplag_match.left_html.current_path
  end

  def run_jplag
    @task = Task.find_by! id: params[:submission_task_id]

    authorize! :moss, @task

    SubmissionTester.run_jplag(@task)

    redirect_to submission_task_jplag_results_path(@task)
  end
end
