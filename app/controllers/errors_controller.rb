class ErrorsController < ApplicationController
  def file_too_large
    flash[:alert] = 'Datei(en) zu groß! Maximal 15Mb!'

    redirect_back fallback_location: root_url
  end
end
