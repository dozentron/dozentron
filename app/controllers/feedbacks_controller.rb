class FeedbacksController < SecureController

  def index

    @feedback = Feedback.new()

  end

  def new
    @submission = Submission.find_by! id: params[:submission_id]
    @feedback = Feedback.new
  end


  def create
    @submission = Submission.find_by! id: params[:submission_id]
    @feedback = Feedback.new(feedback_params)
    @feedback.task = @submission.task
    @feedback.student = @submission.user
    @feedback.editor = @current_user

    if @feedback.save
      enqueue_mail_job

      flash[:success] = 'Feedback wurde erfolgreich erstellt.'
      # redirect_to  [@submission, @feedback]
      respond_to do |format|
        format.html { redirect_to polymorphic_url([@submission.task, :results]) }
        format.json { render json: @feedback }
      end

    else
      flash[:alert] = 'Feedbackerstellung nicht erfolgreich.'

      respond_to do |format|
        format.html { render :new }
        format.json { render json: @feedback }
      end

    end
  end

  def edit
    @feedback = Feedback.find(params[:id])
    @submission = Submission.find_by! id: params[:submission_id]
  end

  def show
    @submission = Submission.find_by! id: params[:submission_id]
    @feedback = Feedback.find(params[:id])

    respond_to do |format|
      format.html
      format.json { render json: @feedback }
    end
  end

  def update
    @submission = Submission.find_by! id: params[:submission_id]
    @feedback.editor = @current_user

    if @feedback.update(feedback_params)
      enqueue_mail_job

      # redirect_to  [@submission, @feedback]
      respond_to do |format|
        format.html { redirect_to polymorphic_url([@submission.task, :results]) }
        format.json { render json: @feedback }
      end
    else
      flash[:alert] = 'Feedbackbearbeitung nicht erfolgreich.'

      respond_to do |format|
        format.html { render :edit }
        format.json { render json: @feedback }
      end
    end
  end

  def destroy
    raise "nope"
  end

  private

  def feedback_params
    params.require(:feedback).permit(:feedback, :markup)
  end

  def enqueue_mail_job
    unless @feedback.mail_enqueued?
      @feedback.update_attributes mail_enqueued: true, mail_enqueued_at: DateTime.now
    end
  end
end
