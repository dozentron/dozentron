
generateMarkupLink = (obj) ->
  {name, url, isImage} = obj
  markup = $('select#markup').val()
  if markup == 'markdown'
    return if isImage then "\n![#{name}](#{url})" else "\n[#{name}](#{url})"
  else if markup == 'asciidoc'
    return if isImage then "\nimage::#{url}[#{name}]" else "\n#{url}[#{name}]"
  else
    console.error("don't know markup,", markup)
    ""



addAttachment = (obj) ->
        str = generateMarkupLink(obj)
        console.log "elem", $('textarea[data-provide="markdown"]')
        area = $('textarea[data-provide="markdown"]')
        area.val(area.val()+str)

handleAttachmentError = (xhr) ->
        alert = '<div class="alert alert-danger">File too large!</div>'

        if xhr.status = 422 #422 = UnprocessableEntity
                $('div.md-editor').append(alert)
        else
                $(alert).text("Oops: #{xhr.statusText}")
                $('div.md-editor').append(alert)

setupAttachmentBtn = ->
        $('input#attachment-upload').on 'change', ->
                fileInput = this
                url = $(fileInput).data('upload-url')
                console.log "changed", this
                attachment = fileInput.files[0]
                formData = new FormData()
                formData.append 'attachment[file]', attachment, attachment.name
                console.log url
                console.log attachment
                console.log formData
                $.ajax({
                        url: url,
                        type: 'POST',
                        data: formData,
                        contentType: false,
                        processData: false,
                        beforeSend: (xhr) ->
                                xhr.setRequestHeader 'X-CSRF-Token', $('meta[name="csrf-token"]').attr('content')
                        success: (res) ->
                                addAttachment res
                        error: (xhr, status, err) ->
                                console.log "error", status
                                console.log xhr
                                handleAttachmentError(xhr)
                        })

setupMarkupSelection = ->
  init_val = $('select#markup').val()
  $('input#hidden_markup').val(init_val)
  $('select#markup').on 'change', ->
    new_val = $('select#markup').val()
    $('input#hidden_markup').val(new_val)

setupLockTaskBtns = ->
  $('.lock-task-btn').on 'click', ->
    elem = $(this)
    $.ajax
      method: "POST"
      url: "/lock_task"
      data:
        user_id: $(this).attr('data-user-id')
        task_id: $(this).attr('data-task-id')
      success: (result) ->
        p = elem.parent().parent()

        if result.locked && !p.hasClass('result-corrected')
          p.removeClass('result-unlocked')
          p.addClass('result-locked')
        else
          p.removeClass('result-locked')
          p.addClass('result-unlocked')
      error: (request, status, error) ->
        alert error
    false

setupCorrectTaskBtns = ->
  $('.correct-task-btn').on 'click', ->
    elem = $(this)
    $.ajax
      method: "POST"
      url: "/correct_task"
      data:
        user_id: $(this).attr('data-user-id')
        task_id: $(this).attr('data-task-id')
      success: (result) ->
        if result.corrected
          elem.parent().parent().removeClass('result-unlocked')
          elem.parent().parent().addClass('result-corrected')
        else
          elem.parent().parent().removeClass('result-corrected')
          elem.parent().parent().addClass('result-locked')
    false

setupFeedbackBtns = ->
  $("#create-update-feedback-btn").on 'click', ->
    if $(this).attr('data-exists') == "true"
      method = "PUT"
    else
      method = "POST"

    submission_id = $(this).attr("data-submission-id")

    $.ajax
      method: method
      url: $(this).attr('data-url')
      data:
        feedback:
          markup: "markdown"
          feedback: $("#feedback-textarea").val()
      success: (result) ->
        $("#feedback-btn-#{submission_id}").attr("data-feedback-id", result.id)
        console.log result
      error: (request, status, error) ->
        alert error

  $('.open-feedback-btn').on 'click', ->
    exists = !isNaN($(this).attr("data-feedback-id"))
    if exists
      url = "/submissions/#{$(this).attr("data-submission-id")}/feedbacks/#{$(this).attr("data-feedback-id")}.json"

      $.ajax
        method: "GET"
        url: url
        success: (result) ->
          $("#feedback-textarea").val result.feedback
          $("#feedback-modal").modal('show')
        error: (request, status, error) ->
          alert error
    else
      url = "/submissions/#{$(this).attr("data-submission-id")}/feedbacks.json"
      $("#feedback-textarea").val ""
      $("#feedback-modal").modal('show')

    $("#create-update-feedback-btn").attr "data-url", url
    $("#create-update-feedback-btn").attr "data-submission-id", $(this).attr("data-submission-id")
    $("#create-update-feedback-btn").attr "data-exists", exists

    false

$(document).ready ->
        setupAttachmentBtn()
        setupMarkupSelection()
        setupLockTaskBtns()
        setupCorrectTaskBtns()
        setupFeedbackBtns()
        console.log "groups ready"
