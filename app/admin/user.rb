ActiveAdmin.register User do
  index do
    selectable_column
    column :id
    column :email
    column :username
    column :display_name
    column :user_class
    column :last_sign_in_at
    column :last_sign_in_ip
    column :created_at
    actions
  end

  controller do
    def update
      if params[:user][:password].blank?
        params[:user].delete("password")
        params[:user].delete("password_confirmation")
      end
      super
    end
  end

  batch_action :update_user_class, form: {
      user_class: [['Student', 'S'], ['Dozent', 'M']]
  } do |ids, inputs|

    User.where(id: ids).update_all inputs
    # inputs is a hash of all the form fields you requested
    redirect_to collection_path, notice: [ids, inputs].to_s
  end
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
permit_params :user_class, :email, :display_name, :password, :password_confirmation
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end


end
