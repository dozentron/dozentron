require 'testers/submission_tester_languages'
require 'moss_ruby'
require 'string_io'
require 'jplag'

class SubmissionTester
  def self.run_test(submission, test_suit)
    task = test_suit.task
    tester = task.test_runner

    begin
      raw_result = tester.test task, submission, test_suit.name
    rescue StandardError => e
      Rails.logger.error "Error occured while running task:#{task.id}, submission:#{submission.id}, suite:#{test_suit.name}:\n#{e.backtrace}"
      exception = e
    end

    Task.transaction do
      test_suit.test_cases.each do |test_case|
        test_result = TestResult.new
        test_result.submission = submission
        test_result.test_case = test_case

        test_case_name = test_case.name

        if  exception
          test_result.failure_message = exception.message
          test_result.failure_exception = exception.message
          test_result.failure_trace = "Most likely an encoding or internal error:\n#{exception.message}"
        elsif raw_result.nil?
          test_result.failure_message = 'Fatal Error'
          test_result.failure_exception = 'FatalError'
          test_result.failure_trace = ''
        elsif raw_result == 'Timeout exceeded'
          test_result.failure_message = raw_result
          test_result.failure_exception = 'Timeout'
          test_result.failure_trace = raw_result
        else
          result = raw_result[test_case_name]

          if result.nil?
            test_result.failure_message = 'Test timeout'
            test_result.failure_exception = 'Timeout'
          else
            test_result.successful = result['successful']
            test_result.failure_message = result['failureMessage']
            test_result.failure_exception = result['failureException']
            test_result.failure_trace = result['failureTrace']
          end
        end

        test_result.failure_exception.delete! "\u0000" unless test_result.failure_exception.nil?
        test_result.failure_message.delete! "\u0000" unless test_result.failure_message.nil?
        test_result.failure_trace.delete! "\u0000" unless test_result.failure_trace.nil?

        test_result.save
      end
    end
  end

  def self.run_jplag(submission_test)
    tester = submission_test.test_runner
    prepare_and_run_jplag tester, submission_test
  end

  def self.run_moss(submission_test, moss_run)
    tester = submission_test.test_runner

    moss_result = prepare_and_run_moss tester, submission_test

    MossMatch.transaction do
      moss_run.moss_matches.destroy_all

      moss_result.each { |match|
        Rails.application.logger.log "moss returned unexpected result #{match}" unless match.count == 2
        moss_match = moss_run.moss_matches.build
        moss_match.submission_1_id = File.basename(match[0][:filename]).to_i
        moss_match.submission_2_id = File.basename(match[1][:filename]).to_i
        moss_match.match_percentage_1 = match[0][:pct]
        moss_match.match_percentage_2 = match[1][:pct]
        submission_1_html = StringIO.new(prepare_moss_html match[0][:html])
        submission_1_html.original_filename = moss_match.submission_1_id.to_s
        moss_match.submission_1_html = submission_1_html
        submission_2_html = StringIO.new(prepare_moss_html match[1][:html])
        submission_2_html.original_filename = moss_match.submission_2_id.to_s
        moss_match.submission_2_html = submission_2_html
        moss_match.save
      }
    end
  end

  private
  def self.prepare_and_run_jplag(tester, submission_test)
    Dir.mktmpdir do |tmp|
      submission_test.submissions.each do |submission|
        system("unzip #{submission.submission} -d #{File.join(tmp, "#{submission.id}")}")
      end

      system("unzip #{submission_test.test.file.file} -d #{File.join(tmp, "base_dir")}")

      JPlag::Runner.run submissions: tmp, basecode: 'base_dir' do |result|
        jplag_run = JplagRun.find_or_create_by(task_id: submission_test.id)
        jplag_run.jplag_groups.destroy_all

        result.each do |group|
          group_model = jplag_run.jplag_groups.create submission_id: group.name

          group.matches.each do |match|
            group_model.jplag_matches.create submission_id: match.name, percentage: match.percentage, left_html: File.new(File.join(result.dir, match.left_match_html)), right_html: File.new(File.join(result.dir, match.right_match_html))
          end
        end
      end
    end
  end

  def self.prepare_and_run_moss(tester, submission_test)
    Dir.mktmpdir do |dir|
      submission_test.submissions.each do |submission|


        submission_dir = File.join dir, 'submissions', submission.id.to_s
        FileUtils.mkpath submission_dir
        tester.source_files submission.submission do |filename, io|
          i = 0
          current_filename = filename
          while File.exist?(File.join submission_dir, current_filename)
            i = i+1
            current_filename = "#{filename}#{i}"
          end
          FileUtils.touch File.join(submission_dir, current_filename)
          File.open File.join(submission_dir, current_filename), "w" do |f|
            FileUtils.copy_stream io, f
          end
        end
      end

      test_dir = File.join dir, 'test_base'
      FileUtils.mkpath test_dir
      tester.source_files submission_test.test do |filename, io|
        i = 0
        current_filename = filename
        while File.exist?(File.join test_dir, current_filename)
          i = i+1
          current_filename = "#{filename}#{i}"
        end
        FileUtils.touch File.join(test_dir, current_filename)
        File.open File.join(test_dir, current_filename), "w" do |f|
          FileUtils.copy_stream io, f
        end
      end

      raise "No 'moss_user_id' set in secrets.yml.example!" unless Rails.application.secrets.moss_user_id

      moss = MossRuby.new(Rails.application.secrets.moss_user_id)

      # Set options  -- the options will already have these default values
      moss.options[:max_matches] = 50
      moss.options[:directory_submission] =  true
      moss.options[:show_num_matches] = 250
      moss.options[:experimental_server] =    false
      moss.options[:comment] = ""
      moss.options[:language] = submission_test.moss_language

      # Create a file hash, with the files to be processed
      to_check = MossRuby.empty_file_hash
      MossRuby.add_file(to_check, File.join(dir, 'submissions', '*', '*'))
      MossRuby.add_base_file(to_check, File.join(dir, 'test_base', '*'))

      # Get server to process files
      url = moss.check to_check, Proc.new {|s| puts s}

      moss.extract_results url
    end
  end

  def self.prepare_moss_html(html)
    doc = Nokogiri::HTML(html)

    fonts = doc.xpath '//font'
    fonts.each do |font|
      font.name = 'div'
      color = font[:color]
      font.remove_attribute 'color'
      rgb = html_rgb_to_rgb_array color

      rgb << 0.1 # alpha

      font[:style] = "background-color: rgba(#{rgb.join ', '});"
    end

    pre = doc.at_xpath '//pre'
    new_pre = Nokogiri::XML::Node.new 'pre', doc
    pre.add_next_sibling(new_pre)

    pre.name = 'code'
    #TODO language whatever
    pre[:class] = 'language-java'
    pre.parent = new_pre

    return new_pre.to_s
  end

  def self.html_rgb_to_rgb_array(rgb)
    rgb[0] = '' if rgb.start_with? '#'

    rgb.scan(/../).map do |c|
      Integer(c, 16)
    end
  end
end
