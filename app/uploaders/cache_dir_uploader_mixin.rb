module CacheDirUploaderMixin
  def cache_dir
    Rails.configuration.temp_directory
  end
end