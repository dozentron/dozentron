class RunMossJob < ActiveJob::Base
  def perform(moss_run_id)
    @moss_run = MossRun.find_by! id: moss_run_id
    @task = @moss_run.task

    begin
      @moss_run.update_attributes running: true
      SubmissionTester.run_moss @task, @moss_run
    ensure
      @moss_run.update_attributes scheduled: false, running: false
    end
  end
end