class RerunTestJob < ActiveJob::Base
  def perform(task_id)
    @task = Group.find_by! id: task_id

    @task.submissions.each do |submission|
      unless submission.save validate: false
        # TaskUpdateMailer.submission_invalidation(self, submission).deliver_later
      end
    end
  end
end
