class RunSubmissionTestJob < ActiveJob::Base
  def perform(submission_id, test_suit_id)
    @submission = Submission.find_by! id: submission_id
    @test_suit = TestSuit.find_by! id: test_suit_id

    begin
      SubmissionTester.run_test @submission, @test_suit
    rescue Timeout::Error
      raise 'Needs Refactoring' # Setzte die Testergebnisse alle auf Timeout
      @submission.validation_message = "Submission #{@submission.name} lief #{Delayed::Worker.max_run_time}s ohne Ergebnis!"
      @submission.save
    end
  end
end