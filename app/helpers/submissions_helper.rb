module SubmissionsHelper
  def queue_size
    Delayed::Job.where(locked_by: nil, last_error: nil).count
  end

  def load_high? volume=nil
    volume ||= queue_size
    volume > Rails.configuration.load_volume
  end
end
