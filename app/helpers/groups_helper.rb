module GroupsHelper
  def render_text(group)
    if group.markdown?
      render_markdown(group.text_content)
    elsif group.asciidoc?
      Asciidoctor.convert group.text_content,  safe: :safe, attributes: ["source-highlighter=rouge"]
    else
      Rails.logger.warning("Don't know how to render markup: #{group.markup}")
      render_markdown(group.text_content)
    end
  end
end
