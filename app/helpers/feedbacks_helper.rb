module FeedbacksHelper
  def render_feedback(feedback)
    if feedback.markdown?
      render_markdown(feedback.feedback)
    elsif feedback.asciidoc?
      Asciidoctor.convert feedback.feedback,  safe: :safe, attributes: ["source-highlighter=rouge"]
    else
      Rails.logger.warning("Don't know how to render markup: #{feedback.markup}")
      render_markdown(feedback.feedback)
    end
  end
end
