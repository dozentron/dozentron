module InputOutputTasksHelper
  include TasksHelper

  def map_i_o_languages_to_select_options_with_help_path(current_selection)
    map_languages_to_select_options_with_help_path(InputOutputTesterLanguages, current_selection)
  end
end
