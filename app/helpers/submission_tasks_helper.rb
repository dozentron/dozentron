module SubmissionTasksHelper
  include TasksHelper

  def moss_graph_nodes(submissions)
    submissions.map { |submission| {id: submission.id, label: submission.display_name}}.to_json
  end

  def moss_graph_edges(moss_matches)
    moss_matches.map do |moss_match| {
        from: moss_match.submission_1_id,
        to: moss_match.submission_2_id,
        width: (moss_match.match_percentage_1 + moss_match.match_percentage_2) / 30
    }
    end.to_json
  end

  def map_submission_languages_to_select_options_with_help_path(current_selection)
    map_languages_to_select_options_with_help_path(SubmissionTesterLanguages, current_selection)
  end

end
