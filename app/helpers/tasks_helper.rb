module TasksHelper

  def map_languages_to_select_options_with_help_path(tester_languages, current_selection)
    select_options = tester_languages.supported_language_names.map do |language_name|
      [language_name, {'data-path'.to_sym => tester_languages.runner_for_language(language_name).new.teacher_help_path }]
    end

    options_for_select(select_options, current_selection)
  end

  def table_result_class(submission, task)
    klass = submission.user.has_locked_task?(@task) ? 'result-locked' : 'result-unlocked'

    lock = TaskLock.find_by(user_id: submission.user.id, task_id: task.id)
    unless lock.nil?
      if lock.corrected?
        klass = 'result-corrected'
      end
    end

    klass
  end
end
