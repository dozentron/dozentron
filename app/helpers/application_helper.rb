module ApplicationHelper
  def render_markdown(text)
    return nil if text.nil?

    render_options = {
      filter_html: true,
      hard_wrap: false,
      link_attributes: { rel: 'nofollow' }
    }
    renderer = MarkdownHelper.new(render_options)

    extensions = {
      autolink: true,
      fenced_code_blocks: true,
      lax_spacing: true,
      no_intra_emphasis: true,
      strikethrough: true,
      tables: true,
      #superscript: true
    }
    @@markdown_renderer ||= Redcarpet::Markdown.new(renderer, extensions)
    raw @@markdown_renderer.render(text).force_encoding('UTF-8')
  end

  def is_tutor(group, user)
    accesses = group.root.accesses

    return false if user.nil?
    return true if user == group.user
    return accesses.pluck(:user_id).include? user.id
  end

  def icon icon
    make_icon icon
  end

  def make_icon icon
    content_tag 'i', '', class: "fa fa-#{icon}"
  end

  def make_icon_for_boolean value
    if value
      content_tag 'div', class: 'text-success' do
        make_icon 'ok'
      end
    else
      content_tag 'div', class: 'text-danger' do
        make_icon 'remove'
      end
    end
  end

  def make_icon_button text, url, icon, options = {}
    icon = "fa fa-#{icon}"
    link_to(url, options) do
      raw "#{content_tag("span", nil, class: icon)} #{text}"
    end
  end

  def make_tooltip text, tooltip
    raw "<a data-toggle=\"tooltip\" data-original-title=\"#{tooltip}\">#{text}</a>"
  end

  def icon_link_to name, path, icon, *args
    options = args.extract_options!

    link_class = options.delete(:class)

    name = " #{name}" if name

    link_to path, {class: link_class}.update(options) do
      raw make_icon(icon) + name
    end
  end

  def btn_link_to(name, path, *args)
    options = args.extract_options!
    html_class = "btn " + (options.delete(:class) || 'btn-default')
    icon = options.delete(:icon)

    if icon
      if icon.is_a? Hash
        icon_class = icon[:class]
        icon = icon[:icon]
      end
      span = content_tag("span", nil, class: "fa fa-#{icon} #{icon_class}") if icon

      if name && !name.empty?
        name = "#{span} #{name}".html_safe
      else
        name = "#{span}".html_safe
      end
    end

    link_to name, path, {class: html_class}.update(options)
  end

  def make_sort_link_with_icon title, filter
    params[:direction] = 'asc' unless params[:direction] == 'desc'

    if params[:filter] == filter
      link_to params.merge({filter: filter, direction: params[:direction] == 'asc' ? 'desc' : 'asc'}) do
        "#{title} #{params[:direction] == 'asc' ? make_icon('sort-asc') : make_icon('sort-desc')}".html_safe
      end
    else
      link_to params.merge({filter: filter, direction: 'asc'}) do
        "#{title} #{make_icon('sort')}".html_safe
      end
    end
  end
end

class MarkdownHelper < Redcarpet::Render::HTML
  include Rouge::Plugins::Redcarpet
  # def list(contents, list_type)
  #   if list_type.eql? :unordered
  #     %(<ul class="browser-default">#{contents}</ul>)
  #   elsif list_type.eql? :ordered
  #     %(<ol class="browser-default">#{contents}</ol>)
  #   else
  #     %(<#{list_type} class="browser-default">#{contents}</#{list_type}>)
  #   end
  # end

  def table(header, body)
    '<span class="markdown-body">' \
    '<table class="table">' \
    "#{header}#{body}" \
    '</table>' \
    '</span>'
  end
end
