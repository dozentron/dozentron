module RevisionHelper
  def dozentron_version
    file = Pathname.new "#{Rails.root}/24eb9cc415ed21528cc4721bd18007888fd9dcd7"
    if File.exist?(file) and File.file?(file)
      revision = File.read(file).trim
      {name: revision, display: revision[0..8], link: true}
    else
      {name: "development", display: "dev", link: false}
    end
  end
end
