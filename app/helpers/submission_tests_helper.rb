module SubmissionTestsHelper
  def prepare_index_json(submission_tests)
    data = submission_tests.map do |submission_test|
      {
          name: submission_test.submission.file.basename,
          created_at: distance_of_time_in_words_to_now(submission_test.created_at),
          num_test_results: submission_test.test_results.count,
          num_failures: submission_test.test_results.where(correct: false).count,
          progress: submission_test.progress_percent,
          path: submission_test_path(submission_test, anchor: 'details')
      }
    end

    data.to_json
  end
end
