
## this module isn't used anymore; consider removing it if there is no interesed in it
## it was considered for finding separate documentation pages based on their filename.
## date: 2018-10-10 (N. Justus)

module DocumentationHelper
  DOCUMENTATION_ROOT = Pathname.new "#{Rails.root}/doc"
  def find_file pattern
    file = Dir[(DOCUMENTATION_ROOT+pattern).to_s+"*"][0]
    file
  end

  def pages
    files = Dir[DOCUMENTATION_ROOT.to_s+"/**/*.{md,adoc}"].map { |f| Pathname.new(f).relative_path_from(DOCUMENTATION_ROOT).to_s }
    files.map { |path|
      {title: path, url: doc_path+"/"+path}
    }
  end

  def is_markdown(file)
    extension = File.extname(file)
    extension == '.md'
  end
end
