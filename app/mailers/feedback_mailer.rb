class FeedbackMailer < ApplicationMailer
  def feedback_mail(feedback_id)
      @feedback = Feedback.find feedback_id
      @user = @feedback.student
      @task = @feedback.task
      @submission = @task.submissions.find_by_user_id @feedback.student_id
      mail(from: "no-reply@dozentron.mni.thm.de", to: @user.email, subject: "AuD Dozentron: Sie haben ein Feedback erhalten")
  end
end
