class TaskUpdateMailer < ApplicationMailer
  def submission_invalidation(task, submission)
    @user = submission.user
    @task = task
    @url  = task_path @task
    mail(to: @user.email, subject: "Abgabe für #{@task.title} wurde ungültig!")
  end
end
