# Add your own tasks in files placed in lib/tasks ending in .rake,
# for example lib/tasks/capistrano.rake, and they will automatically be available to Rake.

require File.expand_path('../config/application', __FILE__)

Rails.application.load_tasks

desc "Generates the user manual/documentation"
task :compile_doc do
  doc_path = Rails.root.join("doc")
  img_path = doc_path.join("img")
  index_path = doc_path.join("index.adoc")
  out = Rails.root.join("public")
  out_file = out.join("documentation.html")

  Asciidoctor.convert_file(index_path.to_s, header_footer: true)
  `bundle exec asciidoctor -o #{out_file} #{index_path}`
  FileUtils.cp_r img_path.to_s, out.to_s

  puts "documentation compiled and created at: #{out_file}"
end

desc "Generates the example tasks"
task :generate_examples => :environment do
  example_root = Rails.root.join("examples")
  tmp_dir = Rails.root.join("tmp")
  yamls = Dir["#{example_root}/**/*.yml"].map{ |p| YAML.load_file(p) }
  uid = User.first.id
  root_title = "Dozentron Task examples"

  #remove existing examples, if any
  search = Group.find_by(title: root_title)
  if search
    search.children.delete_all
    search.delete
  end
  examples = Group.create!(title: root_title,
                             text_content: "this group contains example tasks for dozentron.",
                             user_id: uid)

  tasks = yamls.map do |yml|
    clazz = Object.const_get yml['type']

    task = clazz.create!(title: yml['title'],
                        user_id: uid,
                        text_content: yml['description'],
                        deadline: (Time.now+20.years),
                        language: yml['language'],
                        visible: true,
                        parent: examples,
                        test: example_root.join(yml['test']).open,
                        stub: example_root.join(yml['stub']).open
                       )
    task
  end
  puts "Examples created, parent group is: #{examples.title}"
end
