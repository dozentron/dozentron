== Aufgabentypen

In diesem Abschnitt sind alle bisher unterstützen oder geplanten Aufgabentypen aufgelistet.

:leveloffset: +1

include::junit.adoc[]

include::io.adoc[]

include::jshell.adoc[]

include::python.adoc[]

:leveloffset: -1
