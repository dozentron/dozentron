== jar-Archiv erstellen

Die folgende Anleitung erklärt wie man in
https://www.jetbrains.com/idea[IntelliJ Idea]
ein jar-Archiv erstellen kann.
Die erstellte jar kann danach in Dozentron abgegeben werden.

image::jar_erstellen/start.png[]

Öffne das IntelliJ Projekt, für das eine Jar-Datei erstellt werden soll.

image::jar_erstellen/file-menu.png[]

Öffne das Menü: `File > Project Structure`.

image::jar_erstellen/project-structure-artifacts.png[]

Navigiere zu dem Menü `Artifacts` auf der linken Seite.

image::jar_erstellen/project-structure-artifacts-add.png[]

Öffne über das grüne + das Auswahl Menü für die Artifacts.

image::jar_erstellen/artifacts-jar.png[]

Wähle die Option `Jar > from module with dependencies`.

image::jar_erstellen/add-artifact-dialog.png[]

Bestätige das Erstellen des Artefaktes mit Standardeinstellungen.

image::jar_erstellen/add-jar-content.png[]

Öffne über das grüne + das Menü um zusätzliche Inhalte in die Jar-Datei zu packen.

image::jar_erstellen/add-jar-content-dir.png[]

Wähle die Option `Directory Content`.

image::jar_erstellen/add-jar-content-dir-src.png[]

Wähle in dem Pop-up, das sich geöffnet hat den Ordner aus, der die Java-Dateien enthält. Dies ist standardmäßig der Ordner `src`. Wähle **NICHT** den Ordner, der das Projekt enthält, sonst werden viele unnötige Dateien in die Jar-Datei gepackt. Danach bestätige die Auswahl und schließe das Pop-up.

image::jar_erstellen/menu-build-artifacts.png[]

Um die Jar-Datei zu bauen, navigiere zu `Build > Build Artifacts...`.

image::jar_erstellen/artifact-build.png[]

In dem Pop-up das sich geöffnet hat die Aktion `Build` auswählen.

image::jar_erstellen/finish.png[]

Herzlichen Glückwunsch. Die fertige Jar-Datei wird in dem Ordner `out/artifacts/...` abgelegt.
