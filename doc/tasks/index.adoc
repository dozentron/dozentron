== Aufgaben

_Aufgaben_ sind die typischen Studentenabgaben: Der Dozent stellt unit-Tests und eine Vorgabe zur Verfügung und die Studenten
geben ein Archiv (eine jar) ab.

:leveloffset: +1

include::jar/index.adoc[]

include::aufgabentypen/index.adoc[]

// include::junit.adoc[]

// include::io.adoc[]

// include::jshell.adoc[]

// include::python.adoc[]

:leveloffset: -1
