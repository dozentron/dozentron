= Dozentron Documentation

This directory contains the documentation for dozentron.
Written in asciidoc, the documentation's html is generated ones during deployment.

== Generating html file
You can generate a local html file of the documentation through:
[source, sh]
----
$ bundle exec asciidoctor index.adoc
----
This generates `index.html`.

== Directory Structure
The whole documentation  is linked in `index.adoc`.
Each subdirectory contains a `index.adoc` file itself, so the parent directory can include
`child/index.adoc` and everything inside of this directory is linked through this file.
Images are placed in the `img` directory.
It's set as the _default path_ for images, so img _can be omitted_, when including images.

----
.
├── howto.adoc #included in /index.adoc
├── img
│   ├── jar_erstellen
│   │   ├── add-artifact-dialog.png #referenced as jar_erstellen/add-artifact-dialog.png
│   └── JUnit-Tests-via-IntelliJ
│       ├── JUnit1.png
├── index.adoc #includes junit/index,adoc, plugins/index,adoc
├── junit
│   ├── index.adoc
│   ├── junit.adoc #linked through junit/index.adoc
│   └── junit-intellij.adoc #linked through junit/index.adoc
├── plugins
│   ├── index.adoc
│   └── java #all linked through plugins/index.adoc
│       ├── jar.adoc
│       ├── jar_erstellen.adoc
└── troubleshooting.adoc
----
