== Aufgabenerstellung

Die Erklärung, wie der Quellcode für die einzelnen Aufgabentypen strukturiert sein muss, findest du im
https://git.thm.de/cslz90/ad-cs/blob/master/2018_ss/anleitungen/GradleHOWTO.md[Gradle-HOWTO].

:leveloffset: +1

include::junit.adoc[]

include::io.adoc[]

include::jshell.adoc[]

include::python.adoc[]

:leveloffset: -1
