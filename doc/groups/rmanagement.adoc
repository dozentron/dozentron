=== Rechte vergeben
Dozentron kennt zur Zeit nur 2 Arten von Rechten/Rollen: _Student_ und _Dozent_.

Studenten::
  dürfen Aufgaben einsehen, Abgaben einreichen und ihre eigene Abgabe mit Ergebnis sehen.

Dozenten::
  dürfen Gruppen/Aufgaben erstellen und Abgaben von _allen Studenten_ einsehen.


Um Tutoren die Dozentenrechte in einer Gruppe zu geben, kann mittels _Rechte verwalten_ innerhalb der Gruppe
einzelnen Studenten Dozentenrechte eingeräumt werden.

.Link zu Rechte verwalten
image::groups/rechte-verwalten.png[]

.Dozentenrechte einzelnen Benutzern zuweisen. In der Suche kann der Name eingegeben werden.
image::groups/rechte-vergeben.png[]
