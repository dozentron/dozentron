require File.expand_path('../boot', __FILE__)

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module AutomatisierteAbgabeTests
  class Application < Rails::Application
    # timezone setting
    config.time_zone = 'Berlin'

    config.application_name = 'Dozentron'
    config.temp_directory = "/tmp/"+config.application_name.downcase+"-"+Rails.env

    # Be sure to have the adapter's gem in your Gemfile
    # and follow the adapter's specific installation
    # and deployment instructions.
    config.active_job.queue_adapter = :delayed_job

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.
    config.action_view.field_error_proc = Proc.new { |html_tag, instance|
      "#{html_tag}".html_safe
    }

    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    # config.i18n.load_path += Dir[Rails.root.join('my', 'locales', '*.{rb,yml}').to_s]
    config.i18n.default_locale = :de
    config.i18n.fallbacks =[:en]

    # Autoload app/testers
    config.autoload_paths << "#{Rails.root}/app/testers"

    config.load_volume = 2
    config.max_attachment_size = 10.megabytes

    config.git_rev = "Dozentron-#{(Rails.env.development? || Rails.env.test?) ? `git rev-parse --short HEAD` : File.read("#{Rails.root}/REVISION").slice(0, 7)}"

    JavaSubmissionTester.utf8_trolling = true
    JavaSubmissionTester.skip_test = false
    JavaSubmissionTester.bwrap = "/usr/bin/bwrap"
    JavaSubmissionTester.timeout = "/usr/bin/timeout"

    JavaSubmissionTester.java_path = {
      java: '/usr/lib/jvm/java-12-oracle/bin/java',
      javac: '/usr/lib/jvm/java-12-oracle/bin/javac',
      javap: '/usr/lib/jvm/java-12-oracle/bin/javap'
    }

    JavaSubmissionTester.bubblewrap_dependencies = [
      "/usr",
      "/lib",
      "/lib64",
      "/sbin"
    ]
  end
end
