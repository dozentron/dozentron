Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  devise_for :users
  root to: 'application#index'

  resources :moss_matches, only: [:index, :show]
  resources :jplag_matches, only: [:show]

  resource :users, only: [] do
    get :autocomplete_display_name, :on => :collection
  end

  resources :groups do
    get :results
    get :statistics
    get :autocomplete_title, :on => :collection
    resources :groups, only: [:new]
    resources :accesses, only: [:index]
    resources :submission_tasks, only: [:new]
    resources :input_output_tasks, only: [:new]
    resources :jshell_tasks, only: [:new]
  end

  scope :groups do
    resources :attachments, only: [:create, :show]
  end

  resources :accesses, only: [:create]

  resources :accesses, only: [:destroy]

  resources :submission_tasks, except: [:new, :index] do
    get :results
    get :statistics
    get :moss_graph
    get :moss_results
    get :jplag_results
    get :download_tests
    get :download_submissions
    get :run_moss, to: 'moss_matches#run_moss'
    get :run_jplag, to: 'jplag_matches#run_jplag'
    resources :submissions, only: [:new]
  end

  resources :input_output_tasks, except: [:new, :index] do
    get :results
    get :statistics
    get :download_submissions
    resources :submissions, only: [:new]
  end

  resources :jshell_tasks, except: [:new, :index] do
    get :results
    get :statistics
    get :download_submissions
    resources :submissions, only: [:new]
  end

  resources :submissions, except: [:new] do
    get :download
    resources :feedbacks, only: [:new,:edit,:show, :update, :create]
  end

  resources :inputs, only: [:show]

  post '/lock_task', to: 'task_lock#toggle'
  post '/correct_task', to: 'task_lock#toggle_corrected'

  post 'submissions/batch_upload', to: 'submissions#batch_upload', as: 'submission_batch_upload'

  get 'errors/file_too_large'

  get 'doc', to: 'documentation#index'
  #get 'doc/*file', to: 'documentation#show'

  mount JavaSubmissionTester::Engine, at: 'java_tester'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  # Serve websocket cable requests in-process
  # mount ActionCable.server => '/cable'
end
