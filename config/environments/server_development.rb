Rails.application.configure do
  # Settings specified here will take precedence over those in config/application.rb.
  Delayed::Worker.delay_jobs = true
  # Settings specified here will take precedence over those in config/application.rb.
  Delayed::Worker.destroy_failed_jobs = false

  # config.web_console.development_only = false #activate rails debug console

  # Code is not reloaded between requests.
  config.cache_classes = true

  # Eager load code on boot. This eager loads most of Rails and
  # your application in memory, allowing both threaded web servers
  # and those relying on copy on write to perform better.
  # Rake tasks automatically ignore this option for performance.
  config.eager_load = true

  # Full error reports are disabled and caching is turned on.
  config.consider_all_requests_local       = false
  config.action_controller.perform_caching = true

  # Disable serving static files from the `/public` folder by default since
  # Apache or NGINX already handles this.
  config.public_file_server.enabled = ENV['RAILS_SERVE_STATIC_FILES'].present?

  # Compress JavaScripts and CSS.
  config.assets.js_compressor = :uglifier
  # config.assets.css_compressor = :sass

  # Do not fallback to assets pipeline if a precompiled asset is missed.
  config.assets.compile = true

  # Asset digests allow you to set far-future HTTP expiration dates on all assets,
  # yet still be able to expire them through the digest params.
  config.assets.digest = true

  # Adds additional error checking when serving assets at runtime.
  # Checks for improperly declared sprockets dependencies.
  # Raises helpful error messages.
  config.assets.raise_runtime_errors = true

  # Raises error for missing translations
  # config.action_view.raise_on_missing_translations = true

  # Use an evented file watcher to asynchronously detect changes in source code,
  # routes, locales, etc. This feature depends on the listen gem.
  # config.file_watcher = ActiveSupport::EventedFileUpdateChecker

  logger           = ActiveSupport::Logger.new(STDOUT)
  logger.formatter = config.log_formatter
  config.logger = ActiveSupport::TaggedLogging.new(logger)

  #bwrap
  JavaSubmissionTester.bubblewrap_dependencies = [
      "/usr",
      "/lib",
      "/lib64",
      "/sbin"
  ]
  JavaSubmissionTester.java_path = {
    java: '/usr/lib/jvm/java-12-oracle/bin/java',
    javac: '/usr/lib/jvm/java-12-oracle/bin/javac',
    javap: '/usr/lib/jvm/java-12-oracle/bin/javap'
  }

end
