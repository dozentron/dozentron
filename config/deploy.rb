# config valid only for current version of Capistrano
lock '3.11'

set :application, 'dozentron'
set :repo_url, 'git@git.thm.de:dalt40/dozentron.git'

set :systemd_use_sudo, true
set :systemd_roles, %w(app)

# Default branch is :master
ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp

# Default deploy_to directory is /var/www/my_app_name
# set :deploy_to, '/var/www/my_app_name'

# Default value for :scm is :git
# set :scm, :git

# Default value for :format is :pretty
# set :format, :pretty

# Default value for :log_level is :debug
# set :log_level, :debug

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
append :linked_files, 'config/puma.rb', 'config/secrets.yml'

# Default value for linked_dirs is []
append :linked_dirs, 'log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'vendor/bundle', 'public/system', 'private'

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
set :keep_releases, 5

set :maintenance_template_path, File.expand_path("../../app/views/maintenance.html.erb", __FILE__)

set :slackistrano, {
    channel: '#server',
    webhook: 'https://hooks.slack.com/services/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'
}


# Number of delayed_job workers
# default value: 1
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# If you update this value you may need to change the systemd service starting workers on boot
# /etc/systemd/system/dozentron-worker.service
set :delayed_job_workers, 2
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

# String to be prefixed to worker process names
# This feature allows a prefix name to be placed in front of the process.
# For example:  reports/delayed_job.0  instead of just delayed_job.0
#set :delayed_job_prefix, 'reports'

# Delayed_job queue or queues
# Set the --queue or --queues option to work from a particular queue.
# default value: nil
#set :delayed_job_queues, ['mailer','tracking']

# Specify different pools
# You can use this option multiple times to start different numbers of workers
# for different queues.
# NOTE: When using delayed_job_pools, the settings for delayed_job_workers and
# delayed_job_queues are ignored.
# default value: nil
#
# Single pool of 3 workers looking at all queues: (when alone, '*' is a
# special case meaning any queue)
# set :delayed_job_pools, { '*' => 3 }
# set :delayed_job_pools, { '' => 3 }
# set :delayed_job_pools, { nil => 3 }
#
# Several queues, some with their own dedicated pools: (symbol keys will be
# converted to strings)
# set :delayed_job_pools, {
#     :mailer => 2,    # 2 workers looking only at the 'mailer' queue
#     :tracking => 1,  # 1 worker exclusively for the 'tracking' queue
#     :* => 2          # 2 on any queue (including 'mailer' and 'tracking')
# }
#
# Several workers each handling one or more queues:
# set :delayed_job_pools, {
#     'high_priority' => 1,                # one just for the important stuff
#     'high_priority,*' => 1,              # never blocked by low_priority jobs
#     'high_priority,*,low_priority' => 1, # works on whatever is available
#     '*,low_priority' => 1,  # high_priority doesn't starve the little guys
#   }
# Identification is assigned in order 0..3.
# Note that the '*' in this case is actually a queue with that name and does
# not mean any queue as it is not used alone, but alongside other queues.

# Set the roles where the delayed_job process should be started
# default value: :app
set :delayed_job_roles, [:app, :background]

# Set the location of the delayed_job executable
# Can be relative to the release_path or absolute
# default value: 'bin'
# set :delayed_job_bin_path, 'script' # for rails 3.x

# To pass the `-m` option to the delayed_job executable which will cause each
# worker to be monitored when daemonized.
set :delayed_job_monitor, true

### Set the location of the delayed_job.log logfile
# default value: "#{Rails.root}/log" or "#{Dir.pwd}/log"
# set :delayed_log_dir, 'path_to_log_dir'

### Set the location of the delayed_job pid file(s)
# default value: "#{Rails.root}/tmp/pids" or "#{Dir.pwd}/tmp/pids"
# set :delayed_job_pid_dir, 'path_to_pid_dir'

namespace :deploy do

  # after :restart, :clear_cache do
  #   on roles(:web), in: :groups, limit: 3, wait: 10 do
  #     # Here we can do anything such as:
  #     # within release_path do
  #     #   execute :rake, 'cache:clear'
  #     # end
  #   end
  # end

  namespace :db do
    desc 'drop all tables in db and setup the db with seed'
    task reset: [:set_rails_env] do
      on fetch(:migration_servers) do
        within release_path do
          with rails_env: fetch(:rails_env) do
            execute :psql, "-c 'drop owned by development'"
            execute :rake, 'db:migrate'
            execute :rake, 'db:setup'
          end
        end
      end
    end

    desc 'reload the database with seed data'
    task seed: [:set_rails_env] do
      on fetch(:migration_servers) do
        within release_path do
          with rails_env: fetch(:rails_env) do
            execute :rake, 'db:seed'
          end
        end
      end
    end

    desc 'backup postgres data'
    task :backup do
      on roles(:db) do |host|
        path = "#{deploy_to}/dumps/"
        filename = "#{path}dozentron.#{release_timestamp}.sql"
        if test "[ ! -d #{path} ]"
          execute :mkdir, "#{path}"
        end
        execute :pg_dump, "#{fetch(:database)} > \"#{filename}\""
      end
    end

    desc 'clean up postgres backups, keeps the last 10 dumps'
    task :cleanup do
      on roles(:db) do |host|
        path = "#{deploy_to}/dumps/"
        puts "Keeping the 10 database dumps"
        execute :ls, "-tp #{path} | grep -v '/$' | tail -n +10 | xargs -I {} rm -- \"#{path}{}\""
      end
    end
  end


  desc 'generate user manual'
  task compile_doc: [:set_rails_env] do
    on release_roles([:db]) do
      within release_path do
        with rails_env: fetch(:rails_env) do
          execute :rake, "compile_doc"
        end
      end
    end
  end
end

before "deploy:migrate", "db:backup"
after "deploy:updated", "deploy:compile_doc"
after  "deploy:cleanup", "db:cleanup"
