class CreateMossRuns < ActiveRecord::Migration[5.0]
  def change
    create_table :moss_runs do |t|
      t.boolean :scheduled, default: false, null: false
      t.boolean :running, default: false, null: false
      t.datetime :last_run

      t.belongs_to :task

      t.timestamps
    end

    add_column :moss_matches, :moss_run_id, :integer
  end
end
