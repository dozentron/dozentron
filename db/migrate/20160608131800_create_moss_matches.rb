class CreateMossMatches < ActiveRecord::Migration[5.0]
  def change
    create_table :moss_matches do |t|
      t.integer :submission_1_id, foreign_key: true
      t.string :submission_1_html
      t.integer :match_percentage_1
      t.integer :submission_2_id, foreign_key: true
      t.string :submission_2_html
      t.integer :match_percentage_2

      t.timestamps
    end
  end
end
