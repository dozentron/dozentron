class AddHiddenTestToGroups < ActiveRecord::Migration[5.0]
  def change
    add_column :groups, :hidden_test, :string
  end
end
