class CreateTestResults < ActiveRecord::Migration[5.0]
  def change
    create_table :test_results do |t|
      t.boolean :successful
      t.string :failure_exception
      t.text :failure_message, limit: 1_073_741_823
      t.text :failure_trace, limit: 1_073_741_823

      t.belongs_to :test_case
      t.belongs_to :submission

      t.timestamps
    end
  end
end
