class AddJplagRunIdtoJplagGroups < ActiveRecord::Migration[5.0]
  def change
    add_column :jplag_groups, :jplag_run_id, :integer
  end
end
