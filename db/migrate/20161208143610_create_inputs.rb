class CreateInputs < ActiveRecord::Migration[5.0]
  def change
    create_table :inputs do |t|
      t.string :input
      t.integer :input_length
      t.belongs_to :user
      t.belongs_to :input_output_task

      t.timestamps
    end
  end
end
