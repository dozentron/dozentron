class CreateTestCases < ActiveRecord::Migration[5.0]
  def change
    create_table :test_cases do |t|
      t.string :name
      t.integer :points
      t.belongs_to :test_suit

      t.timestamps
    end
  end
end
