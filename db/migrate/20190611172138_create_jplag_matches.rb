class CreateJplagMatches < ActiveRecord::Migration[5.0]
  def change
    create_table :jplag_matches do |t|
      t.integer :jplag_group_id
      t.integer :submission_id
      t.string :left_html
      t.string :right_html
      t.string :percentage

      t.timestamps
    end
  end
end
