class CreateJplagGroups < ActiveRecord::Migration[5.0]
  def change
    create_table :jplag_groups do |t|
      t.integer :submission_id

      t.timestamps
    end
  end
end
