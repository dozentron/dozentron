class AddOutputFileToSubmissions < ActiveRecord::Migration[5.0]
  def change
    add_column :submissions, :output, :string
  end
end
