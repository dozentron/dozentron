class CreateFeedbacks < ActiveRecord::Migration[5.0]
  def change
    create_table :feedbacks do |t|

      t.string :feedback

      t.belongs_to :student, foreign_key: {to_table: :users}
      t.belongs_to :editor, foreign_key: {to_table: :users}

      t.belongs_to :task

      t.timestamps
      t.index [:student_id, :task_id], unique: true
    end
  end
end
