class CreateTestSuits < ActiveRecord::Migration[5.0]
  def change
    create_table :test_suits do |t|
      t.string :name
      t.belongs_to :task

      t.timestamps
    end
  end
end
