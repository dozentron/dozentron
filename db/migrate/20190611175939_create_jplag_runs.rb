class CreateJplagRuns < ActiveRecord::Migration[5.0]
  def change
    create_table :jplag_runs do |t|
      t.integer :task_id
      t.boolean :scheduled
      t.boolean :running
      t.datetime :last_run

      t.timestamps
    end
  end
end
