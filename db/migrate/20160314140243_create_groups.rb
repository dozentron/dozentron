class CreateGroups < ActiveRecord::Migration[5.0]
  def change
    create_table :groups do |t|
      t.string :ancestry
      t.string :type

      t.string :title
      t.string :text
      t.belongs_to :user
      t.datetime :deadline
      t.boolean :visible

      t.string :test
      t.string :stub
      t.string :language
      t.boolean :run_moss

      t.timestamps
    end

    add_index :groups, :ancestry
  end
end
