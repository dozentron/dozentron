class AddMailEnqueuedToFeedbacks < ActiveRecord::Migration[5.0]
  def change
    add_column :feedbacks, :mail_enqueued, :boolean, default: false
  end
end
