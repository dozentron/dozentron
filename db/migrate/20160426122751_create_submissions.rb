class CreateSubmissions < ActiveRecord::Migration[5.0]
  def change
    create_table :submissions do |t|
      t.string :name
      t.string :submission, :null => false

      t.belongs_to :task
      t.belongs_to :user

      t.timestamps
    end
  end
end
