class AddMarkupToGroups < ActiveRecord::Migration[5.0]
  def change
    add_column :groups, :markup, :integer, default: 0
  end
end
