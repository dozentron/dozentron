class AddWarningTextToSubmission < ActiveRecord::Migration[5.0]
  def change
    add_column :submissions, :warning_text, :text
  end
end
