class AddBonusToGroups < ActiveRecord::Migration[5.0]
  def change
    add_column :groups, :bonus, :boolean, default: false
  end
end
