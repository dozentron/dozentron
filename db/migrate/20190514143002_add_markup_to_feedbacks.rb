class AddMarkupToFeedbacks < ActiveRecord::Migration[5.0]
  def change
    add_column :feedbacks, :markup, :integer, default: 0
  end
end
