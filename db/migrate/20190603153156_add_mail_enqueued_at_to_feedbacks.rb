class AddMailEnqueuedAtToFeedbacks < ActiveRecord::Migration[5.0]
  def change
    add_column :feedbacks, :mail_enqueued_at, :datetime
  end
end
