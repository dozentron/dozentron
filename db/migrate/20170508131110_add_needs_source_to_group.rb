class AddNeedsSourceToGroup < ActiveRecord::Migration[5.0]
  def up
    change_column :submissions, :submission, :string, :null => true
    add_column :groups, :needs_source, :boolean, default: true
  end

  def down
    change_column :submissions, :submission, :string, :null => false
    remove_column :groups, :needs_source
  end
end
