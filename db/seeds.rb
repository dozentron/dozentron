# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
NUM_SUBMISSION_TESTS_TO_CREATE = 3

MAX_NUM_SUBMISSIONS_TO_CREATE = 10
MIN_NUM_SUBMISSIONS_TO_CREATE = 3

AdminUser.create!(email: 'admin@example.com', password: 'password', password_confirmation: 'password')

if Rails.env != 'production'
  R = Random.new

  delay_jobs = Delayed::Worker.delay_jobs
  Delayed::Worker.delay_jobs = false

  puts '--Create Example User and Groups'

  u = User.create(email: 'admin@example.com', username: Faker::Name.first_name, display_name: Faker::Name.name, password: '12345678', user_class: 'M')
  g = Group.create(title: Faker::Book.title, text_content: Faker::Lorem.paragraph(5), user_id: u.id, visible: true)
  g2 = Group.create(title: Faker::Book.title, text_content: Faker::Lorem.paragraph(5), user_id: u.id, visible: true, parent: g)

  File.open(Rails.root.join 'seed-resources', 'Fake-Abgabe.jar') do |submission_file|
    File.open(Rails.root.join 'seed-resources', 'Fake-Test.jar') do |test_file|
      NUM_SUBMISSION_TESTS_TO_CREATE.times do |i|
        puts "--Create Example SubmissionTest #{i + 1}/#{NUM_SUBMISSION_TESTS_TO_CREATE}"
        st = SubmissionTask.create(title: Faker::Book.title, text_content: Faker::Lorem.paragraph(5), user_id: u.id, visible: true, parent: g2, deadline: (Time.now + 5.days), language: 'Java', test: test_file, stub: submission_file)

        tries = 3

        num_submissions_to_create = MIN_NUM_SUBMISSIONS_TO_CREATE + R.rand(MAX_NUM_SUBMISSIONS_TO_CREATE - MIN_NUM_SUBMISSIONS_TO_CREATE)
        num_submissions_to_create.times do |j|
          begin
            tries -= 1
            puts "--Create Example Submissions #{j + 1}/#{num_submissions_to_create}"
            student = User.create(email: Faker::Internet.email, username: Faker::Name.first_name, display_name: Faker::Name.name, password: '12345678', user_class: 'S')
            Submission.create(user_id: student.id, task_id: st.id, submission: submission_file)
          rescue => e
            if tries >= 0
              retry
            else
              puts "I guess I won't create this one, because I can't find a new unique username. It happens."
            end
          end
        end
      end
    end
  end

  Delayed::Worker.delay_jobs = delay_jobs
end