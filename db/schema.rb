# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20190611180744) do

  create_table "accesses", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "group_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["group_id"], name: "index_accesses_on_group_id"
    t.index ["user_id"], name: "index_accesses_on_user_id"
  end

  create_table "active_admin_comments", force: :cascade do |t|
    t.string   "namespace"
    t.text     "body"
    t.string   "resource_id",   null: false
    t.string   "resource_type", null: false
    t.string   "author_type"
    t.integer  "author_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id"
    t.index ["namespace"], name: "index_active_admin_comments_on_namespace"
    t.index ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id"
  end

  create_table "admin_users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.index ["email"], name: "index_admin_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true
  end

  create_table "attachments", force: :cascade do |t|
    t.string   "file"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "group_id"
    t.index ["group_id"], name: "index_attachments_on_group_id"
  end

  create_table "delayed_jobs", force: :cascade do |t|
    t.integer  "priority",   default: 0, null: false
    t.integer  "attempts",   default: 0, null: false
    t.text     "handler",                null: false
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["priority", "run_at"], name: "delayed_jobs_priority"
  end

  create_table "feedbacks", force: :cascade do |t|
    t.string   "feedback"
    t.integer  "student_id"
    t.integer  "editor_id"
    t.integer  "task_id"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.integer  "markup",           default: 0
    t.boolean  "mail_enqueued",    default: false
    t.datetime "mail_enqueued_at"
    t.index ["editor_id"], name: "index_feedbacks_on_editor_id"
    t.index ["student_id", "task_id"], name: "index_feedbacks_on_student_id_and_task_id", unique: true
    t.index ["student_id"], name: "index_feedbacks_on_student_id"
    t.index ["task_id"], name: "index_feedbacks_on_task_id"
  end

  create_table "groups", force: :cascade do |t|
    t.string   "ancestry"
    t.string   "type"
    t.string   "title"
    t.string   "text"
    t.integer  "user_id"
    t.datetime "deadline"
    t.boolean  "visible"
    t.string   "test"
    t.string   "stub"
    t.string   "language"
    t.boolean  "run_moss"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.boolean  "needs_source", default: true
    t.string   "hidden_test"
    t.integer  "markup",       default: 0
    t.boolean  "bonus",        default: false
    t.index ["ancestry"], name: "index_groups_on_ancestry"
    t.index ["user_id"], name: "index_groups_on_user_id"
  end

  create_table "inputs", force: :cascade do |t|
    t.string   "input"
    t.integer  "input_length"
    t.integer  "user_id"
    t.integer  "input_output_task_id"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.index ["input_output_task_id"], name: "index_inputs_on_input_output_task_id"
    t.index ["user_id"], name: "index_inputs_on_user_id"
  end

  create_table "jplag_groups", force: :cascade do |t|
    t.integer  "submission_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.integer  "jplag_run_id"
  end

  create_table "jplag_matches", force: :cascade do |t|
    t.integer  "jplag_group_id"
    t.integer  "submission_id"
    t.string   "left_html"
    t.string   "right_html"
    t.string   "percentage"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "jplag_runs", force: :cascade do |t|
    t.integer  "task_id"
    t.boolean  "scheduled"
    t.boolean  "running"
    t.datetime "last_run"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "moss_matches", force: :cascade do |t|
    t.integer  "submission_1_id"
    t.string   "submission_1_html"
    t.integer  "match_percentage_1"
    t.integer  "submission_2_id"
    t.string   "submission_2_html"
    t.integer  "match_percentage_2"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.integer  "moss_run_id"
  end

  create_table "moss_runs", force: :cascade do |t|
    t.boolean  "scheduled",  default: false, null: false
    t.boolean  "running",    default: false, null: false
    t.datetime "last_run"
    t.integer  "task_id"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.index ["task_id"], name: "index_moss_runs_on_task_id"
  end

  create_table "roles", force: :cascade do |t|
    t.string   "name"
    t.string   "resource_type"
    t.integer  "resource_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.index ["name", "resource_type", "resource_id"], name: "index_roles_on_name_and_resource_type_and_resource_id"
    t.index ["name"], name: "index_roles_on_name"
    t.index ["resource_type", "resource_id"], name: "index_roles_on_resource_type_and_resource_id"
  end

  create_table "submissions", force: :cascade do |t|
    t.string   "name"
    t.string   "submission"
    t.integer  "task_id"
    t.integer  "user_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.string   "output"
    t.text     "warning_text"
    t.index ["task_id"], name: "index_submissions_on_task_id"
    t.index ["user_id"], name: "index_submissions_on_user_id"
  end

  create_table "task_locks", force: :cascade do |t|
    t.integer  "task_id"
    t.integer  "user_id"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.boolean  "corrected",  default: false
  end

  create_table "test_cases", force: :cascade do |t|
    t.string   "name"
    t.integer  "points"
    t.integer  "test_suit_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.index ["test_suit_id"], name: "index_test_cases_on_test_suit_id"
  end

  create_table "test_results", force: :cascade do |t|
    t.boolean  "successful"
    t.string   "failure_exception"
    t.text     "failure_message",   limit: 1073741823
    t.text     "failure_trace",     limit: 1073741823
    t.integer  "test_case_id"
    t.integer  "submission_id"
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.index ["submission_id"], name: "index_test_results_on_submission_id"
    t.index ["test_case_id"], name: "index_test_results_on_test_case_id"
  end

  create_table "test_suits", force: :cascade do |t|
    t.string   "name"
    t.integer  "task_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["task_id"], name: "index_test_suits_on_task_id"
  end

  create_table "trigrams", force: :cascade do |t|
    t.string  "trigram",     limit: 3
    t.integer "score",       limit: 2
    t.integer "owner_id"
    t.string  "owner_type"
    t.string  "fuzzy_field"
    t.index ["owner_id", "owner_type", "fuzzy_field", "trigram", "score"], name: "index_for_match"
    t.index ["owner_id", "owner_type"], name: "index_by_owner"
  end

  create_table "users", force: :cascade do |t|
    t.string   "email"
    t.string   "encrypted_password",     default: "", null: false
    t.string   "username",                            null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "display_name"
    t.string   "user_class"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["username"], name: "index_users_on_username", unique: true
  end

  create_table "users_roles", id: false, force: :cascade do |t|
    t.integer "user_id"
    t.integer "role_id"
    t.index ["role_id"], name: "index_users_roles_on_role_id"
    t.index ["user_id", "role_id"], name: "index_users_roles_on_user_id_and_role_id"
    t.index ["user_id"], name: "index_users_roles_on_user_id"
  end

end
