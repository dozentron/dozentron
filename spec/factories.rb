FactoryGirl.define do
  sequence(:title) { |n| "Example title #{n}" }
  sequence(:email) { |n| "test#{n}@example.com" }
  sequence(:username) { |n| "username#{n}" }
  sequence(:display_name) { |n| "display_name#{n}" }

  factory :student, class: User do
    username
    display_name
    user_class 'S'
    password 'password'

    email
  end

  factory :docent, class: User do
    username
    display_name
    user_class 'M'
    password 'password'

    email
  end

  factory :admin, class: Admin do
    username
    display_name
    password 'password'

    email
  end

  factory :group, class: Group do
    title
    visible true
    association :user, factory: :docent
  end

  factory :submission_task_without_parent, class: SubmissionTask do
    title
    deadline Time.now + 7.days
    visible true
    run_moss false
    language 'Java'

    factory :submission_task, class: SubmissionTask do
      parent_id { FactoryGirl.create(:group).id }
      test { Rack::Test::UploadedFile.new(Rails.root.join('seed-resources', 'Fake-Test.jar'), 'application/java-archive') }
      stub { Rack::Test::UploadedFile.new(Rails.root.join('seed-resources', 'Fake-Abgabe.jar'), 'application/java-archive') }
    end

    factory :submission_task_without_test_cases, class: SubmissionTask do
      parent_id { FactoryGirl.create(:group).id }
      stub { Rack::Test::UploadedFile.new(Rails.root.join('seed-resources', 'Fake-Test.jar'), 'application/java-archive') }
      test { Rack::Test::UploadedFile.new(Rails.root.join('seed-resources', 'Fake-Abgabe.jar'), 'application/java-archive') }
    end
  end

  factory :input_output_task_without_parent, class: InputOutputTask do
    title
    deadline Time.now + 7.days
    visible true
    run_moss false

    factory :input_output_task_non_executable_generator, class: InputOutputTask do
      language 'Java8'
      parent_id { FactoryGirl.create(:group).id }
      test { Rack::Test::UploadedFile.new(Rails.root.join('seed-resources', 'Fake-input-output-example-solution.jar'), 'application/java-archive') }
      stub { Rack::Test::UploadedFile.new(Rails.root.join('seed-resources', 'Fake-Abgabe.jar'), 'application/java-archive') }
    end

    factory :input_output_task, class: InputOutputTask do
      language 'Java8'
      parent_id { FactoryGirl.create(:group).id }
      test { Rack::Test::UploadedFile.new(Rails.root.join('seed-resources', 'Fake-input-output-example-solution.jar'), 'application/java-archive') }
      stub { Rack::Test::UploadedFile.new(Rails.root.join('seed-resources', 'Fake-input-output-generator.jar'), 'application/java-archive') }
    end

    factory :input_output_junit_task, class: InputOutputTask do
      language 'Java8 Junit4'
      parent_id { FactoryGirl.create(:group).id }
      test { Rack::Test::UploadedFile.new(Rails.root.join('seed-resources', 'Fake-input-output-junit.jar'), 'application/java-archive') }
      stub { Rack::Test::UploadedFile.new(Rails.root.join('seed-resources', 'Fake-input-output-generator.jar'), 'application/java-archive') }
    end
  end

  factory :submission, class: Submission do
    association :user, factory: :student

    factory :submission_task_submission, class: Submission do
      association :task, factory: :submission_task
      submission { Rack::Test::UploadedFile.new(Rails.root.join('seed-resources', 'Fake-Abgabe.jar'), 'application/java-archive') }
    end

    factory :input_output_task_submission, class: Submission do
      association :task, factory: :input_output_task
      output_content '5'
      submission { Rack::Test::UploadedFile.new(Rails.root.join('seed-resources', 'Fake-Abgabe.jar'), 'application/java-archive') }
    end
  end
end