require 'rails_helper'

describe 'Groups' do
  describe 'index' do
    it "should redirect to login if not logged in", route: Rails.application.routes.url_helpers.groups_path do
      logout(:user)
      visit Rails.application.routes.url_helpers.groups_path

      expect(page).to have_current_path(Rails.application.routes.url_helpers.new_user_session_path)
    end

    it "should show groups index", route: Rails.application.routes.url_helpers.groups_path do
      login_as(build(:student), :scope => :user)
      visit Rails.application.routes.url_helpers.groups_path

      expect(page).to have_current_path(Rails.application.routes.url_helpers.groups_path)
    end
  end
end