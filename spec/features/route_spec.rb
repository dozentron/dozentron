require 'rails_helper'
require 'support/route_check'

describe "Route" do
  it "tests all routes" do
    Rails.application.reload_routes!
    route_check = RouteCheck.new(routes: Rails.application.routes.routes, world: RSpec::world)

    route_check.missing_docs.should == Set.new
  end
end