require 'rails_helper'

RSpec.describe InputOutputTask, type: :model do
  describe 'legacy format' do
    it 'should need an parent' do
      submission_task = nil

      File.open(Rails.root.join('seed-resources', 'Fake-Test.jar')) do |test_file|
        File.open(Rails.root.join('seed-resources', 'Fake-Abgabe.jar')) do |stub_file|
          submission_task = build(:input_output_task_without_parent, test: test_file, stub: stub_file)
        end
      end

      submission_task.validate
      expect(submission_task.errors.empty?).to be false
    end

    it 'should create testsuites and testcases' do
      input_output_task = create(:input_output_task)

      expect(input_output_task.persisted?).to be true
      expect(input_output_task.test_suits.count).to eql(1)
      expect(input_output_task.test_cases.count).to eql(1)
    end

    it "shouldn't accept an generator that isn't runnable" do
      input_output_task = build :input_output_task_non_executable_generator

      expect{ input_output_task.save! }.to raise_error ActiveRecord::RecordInvalid
    end

    it 'should generate and input and reuse it' do
      input_output_task = create :input_output_task
      user = create :student
      input = input_output_task.input user
      input2 = input_output_task.input user

      expect(input).to eql(input2)
    end
  end

  describe 'junit format' do
    it 'should create testsuites and testcases' do
      input_output_task = create(:input_output_junit_task)

      expect(input_output_task.persisted?).to be true
      expect(input_output_task.test_suits.count).to eql(1)
      expect(input_output_task.test_cases.count).to eql(2)
    end
  end
end
