require 'rails_helper'

RSpec.describe Submission, type: :model do
  describe 'SubmissionTask submission' do
    it 'should get test results after saving' do
      submission = create(:submission_task_submission)

      expect(submission.task.test_cases.count).to eql(submission.test_results.count)
      # Example contains atleast one test that works
      expect(submission.task.test_results.select{ |test_result| test_result.successful }.count).not_to eql(0)

      # beim erneuten Testen sollten alte testergebnisse beseitigt werden
      submission.save
      expect(submission.task.test_cases.count).to eql(submission.test_results.count)
    end
  end

  describe 'InputOutputTask submission' do
    describe 'legacy' do
      it 'should get test results after saving' do
        submission = create(:input_output_task_submission)

        expect(submission.task.test_cases.count).to eql(submission.test_results.count)

        # beim erneuten Testen sollten alte testergebnisse beseitigt werden
        submission.save
        expect(submission.task.test_cases.count).to eql(submission.test_results.count)
      end
    end

    describe 'junit' do
      it 'should get test results after saving' do
        submission = create(:input_output_task_submission, task: create(:input_output_junit_task))

        expect(submission.task.test_cases.count).to eql(submission.test_results.count)

        # beim erneuten Testen sollten alte testergebnisse beseitigt werden
        submission.save
        expect(submission.task.test_cases.count).to eql(submission.test_results.count)
      end
    end
  end
end
