require 'rails_helper'

RSpec.describe SubmissionTask, type: :model do
  it 'should need an parent' do
    submission_task = nil

    File.open(Rails.root.join('seed-resources', 'Fake-Test.jar')) do |test_file|
      File.open(Rails.root.join('seed-resources', 'Fake-Abgabe.jar')) do |stub_file|
        submission_task = build(:submission_task_without_parent, test: test_file, stub: stub_file)
      end
    end

    submission_task.validate
    expect(submission_task.errors.empty?).to be false
  end

  it 'should create testsuites and testcases' do
    submission_task = create(:submission_task)

    expect(submission_task.persisted?).to be true
    expect(submission_task.test_suits.count).to eql(1)
    expect(submission_task.test_cases.count).to eql(9)
  end

  it "shouldn't create test if test doesn't contain junit test cases" do
    submission_task = build :submission_task_without_test_cases

    expect{ submission_task.save! }.to raise_error ActiveRecord::RecordInvalid
  end
end
