class RouteCheck
  attr_reader :routes, :world

  def initialize(routes: nil, world: nil)
    @routes = routes
    @world = world
  end

  def filtered_routes
    collect_routes do |route|
      next if route.internal?
      next if route.verb.blank?
      next if route.controller =~ /^devise.*/
      next if route.path =~ /^\/admin.*/
      route
    end.compact
  end

  def api_specs
    world
        .filtered_examples
        .values
        .flatten
        .reject{ |g| g.metadata.fetch(:route, :not_found) == :not_found }
  end

  def missing_docs
    existing_routes = Set.new(matchable_routes(filtered_routes))
    existing_route_specs = Set.new(matchable_specs(api_specs))

    existing_routes - existing_route_specs
  end

  private
  def matchable_routes(routes)
    routes.collect do |r|
      ::Route.new(r.path[/\/[^( ]+/]) unless r.path[/\/[^( ]+/].nil?
    end.compact
  end

  def matchable_specs(specs)
    specs.map do |spec|
      ::Route.new(spec.metadata[:route])
    end
  end

  def collect_routes
    routes.collect do |route|
      route = yield ActionDispatch::Routing::RouteWrapper.new(route)
    end
  end
end

class ::Route < Struct.new(:path)
  def eql? other
    self.hash == other.hash
  end
  def == other
    path.downcase == other.path.downcase
  end
  def hash
    path.downcase.hash
  end
end