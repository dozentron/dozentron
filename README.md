# Dozentron

Dozentron ist eine Rails-Applikation zum automatischen Testen von Programieraufgaben. Es wurde für Linux mit Ruby 2.4.1 geschrieben.

### Installation
Um Dozentron an einem Lokalen System zu installieren wird Ruby 2.4.1 benötigt.
Dozentron verwendet eine sqlite DB im development und postgres in production.

```bash
cd <project-dir>
gem install bundler
bundle install
cp config/database.yml.example config/database.yml
cp config/secrets.yml.example config/secrets.yml
rails db:create db:migrate db:seed
```

Den postgres Datenbank-Treiber kann man überspringen, indem `bundle install --without production server_development` verwendet wird.
Dann läuft Dozentron mit sqlite und es werden keine native-extensions für postgres benötigt.
Test dependencies können mittels `--without test` ignoriert werden.

Um Moss verwenden zu können, muss in der Datei `config/secrets.yml` eine Moss-Benutzerkennung angegeben werden.

```yaml
secrets.yml.example
development:
  moss_user_id:
  #...
```

Für den standard Java-Support wird
[bubblewrap](https://www.archlinux.org/packages/extra/x86_64/bubblewrap/),
Java 8, Java 9 (ja derzeit beide!) und [timeout](https://linux.die.net/man/1/timeout) benötigt.
Die Pfade für bubblewrap und java müssen dem System-entsprechend in der `config/application.rb` angepasst werden.

Nach der Einrichtung, startet man einen Rails-Server mit dem Befehl:
```bash
rails s
```

Worker zum ausführen der Tests startet man mit:
```bash
bin/delayed_job
```

## Beispiele generieren
Dozentron kommt mit einer Reihe von Beispielaufgaben, die im examples Ordner liegen.
Sie können mittels `bundle exec rake generate_examples` in Dozentron eingetragen werden.
Dies erzeugt die Aufgabengruppe *Dozentron Task examples*.

## Deployment
Dozentron verwendet capistrano zum deployen.
Ein neues Release wird mittels: `cap [server-version] deploy` erstellt (Auf dem lokalen System ausführen).
[server-version] ist eine von `production`, `development` oder `test`.

Database-Seeds können auf dem Server mittels `cap development deploy:db:seed` erstellt werden.
Wenn der Service nicht startet, kann es sein dass `puma` nicht installiert ist.
Installiere es manuell mittels `gem install puma`.

### !! ACHTUNG:
In einem Produktionssystem muss das cachen von ruby Klassen aktiviert sein.
Ansonsten verbraucht *delayed jobs* viel zu viele Resourcen.
Man achte auf diese Einstellung in dem verwendeten Environment (`config/environments/*.rb`):
``` ruby
config.cache_classes = true
```


### Amdmin Panel/Dozenten erstellen
Benutzer und Gruppen können über das Adminpanel (`locahost:3000/admin/users`) eingetragen werden.
Default E-mail: `admin@example.com` Password: `password` (siehe: `db/seeds.rb`).
Angemeldeten Benutzern können Dozentenrechte zugewiesen werden, indem die `User Class` auf `M` gesetzt wird.

### Admin Panel - Failed Jobs
Fehlgeschlagene Jobs können im Admin Panel detailierte betrachtet werden.
Der Grund des Fehlschlags steht in der Delay Jobs Tabelle.

## Deployment mit Capistran


### Support für Programmiersprachen
Dozentron unterstützt das Testen von Programmiersprachen über Plugins. Dadurch kann Dozentron theoretisch mit jeder Programmiersprache arbeiten.

### Dozentron Manual

Die Hilfeseiten/das Manual liegt im [`doc` Ordner](doc/readme.adoc).
Das Manual kann auch über den Hilfe-Link innerhalb von Dozentron aufgerufen werden, wenn vorher die html-Seite generiert wurde:

```sh
$ bundle exec rake compile_doc
```

(Während dem Deployment mittels Capistrano, wird die html-Seite automatisch generiert.)

### Wiki
Mehr informationen findet man in der [Wiki](https://git.thm.de/dalt40/dozentron/wikis/home) zu Dozentron.
