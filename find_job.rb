

regexp = /arguments:\s+-\s+(\d+)\s+-\s+(\d+)\s+locale/
handlers = Delayed::Job.where(locked_by: nil, last_error: nil).pluck(:id, :handler).map  { |(id,str)| [id, YAML::load(str).job_data] }

handlers_submissions = handlers.map do |(id,data)|
  [id, data["arguments"][0]]
end
to_delete = handlers_submissions.filter { |(id, sub_id)| Submission.where(id: sub_id, task_id: self.task_id, self.user_id).exists? }
to_delete.each do |id|
  Delayed::Job.delete(id)
end
