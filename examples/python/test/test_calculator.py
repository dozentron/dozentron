import unittest
from calculator import Calculator

class TestCalculator(unittest.TestCase):
    def test_add(self):
        c = Calculator()
        self.assertEqual(c.add(5,4), 9)
        self.assertNotEqual(c.add(5,4), 8)
    def test_mul(self):
        c = Calculator()
        self.assertEqual(c.mul(10,2), 20)
    def test_always_error(self):
        c = Calculator()
        self.assertEqual(1,c.error(1))
