require 'testers/language_already_registered_error'

class SubmissionTesterLanguages
  @@supported_languages = {}

  def self.registerLanguage!(language_name, test_runner_class)
    raise LanguageAlreadyRegisteredError, "#{language_name} was already registered!" if @@supported_languages.has_key? language_name

    @@supported_languages[language_name] = test_runner_class
    Rails.logger.info "registered language: #{language_name}"

    return true
  end

  def self.supported_language_names
    @@supported_languages.keys
  end

  def self.runner_for_language(language)
    @@supported_languages[language]
  end
end