# Interface for a test runner.
module ITestRunner
  # Route to help texts for students.
  def student_help_path; end
  # Route to help texts for docents.
  def teacher_help_path; end
  # The language that moss uses to check submissions.
  def moss_language; end
  # Provides all source files that may be relevant for moss to the given block.
  def source_files(carrierwave_uploader); end

  # Finds all TestSuites from the given testZips.
  # A TestSuite is a set of multiple test methods, usually some kind of class extending TestSuite.
  # @param testZips a list of carrierwave_uploaders containing the tests
  # @param stub_file the stub file (usually a archive) as carrierwave_uploader that could be needed to compile the sources
  # @return a list of TestSuite names
  def test_suit_names_for(testZips, stub); end

  # Finds all TestCases in the given TestSuite.
  # A TestCase is a method that tests some code and lives inside of a TestSuite.
  # @param testZips a list of carrierwave_uploaders containing the tests
  # @param stub the stub file (usually a archive) as carrierwave_uploader that could be needed to compile the sources
  # @param test_suit_name name of the TestSuite
  # @return a map with the following schema:
  # => {
  #     <test_mehod>: {
  #       extras: {}
  #     }
  #   }
  def test_cases_for_testsuit(testZips, stub, test_suit_name); end

  # Validates the submission from a student.
  # @param test_file the test file (usually a archive) as carrierwave_uploader
  # @param stub_file the stub file (usually a archive) as carrierwave_uploader that could be needed to compile the sources
  # @return a string message if structure isn't valid, nil otherwise
  def validate_submission_structure(submission_file, test_file); end

  # Validates the structure of a test.
  # @param test_file the test file (usually a archive) as carrierwave_uploader
  # @param stub_file the stub file (usually a archive) as carrierwave_uploader that could be needed to compile the sources
  # @return a string message if structure isn't valid, nil otherwise
  def validate_test_structure(test_file, stub_file); end

  # Executes the given test_suit for the given submission.
  # @param task the full task model
  # @param submission the students submission model
  # @param test_suit test_suit to run
  # @return a map with the following schema:
  # => {
  #     <test_mehod>: {
  #       failureTrace: <string>
  #       failureMessage: <string>
  #       failureException: <string>
  #       successful: <boolean>
  #     }
  #   }
  def test(task, submission, test_suit); end
end
