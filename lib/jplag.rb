require 'tmpdir'
require 'csv'

module JPlag
  JPLAG_BIN="jplag.jar".freeze
  JAVA_BIN="java".freeze

  class Error < StandardError; end

  class Match
    attr_reader :group, :name, :index, :percentage

    def initialize(options={})
      @group = options[:group]
      @name = options[:name]
      @index = options[:index]
      @percentage = options[:percentage]
    end

    def left_match_html
      "match#{@index}-0.html"
    end

    def right_match_html
      "match#{@index}-1.html"
    end
  end

  class MatchGroup
    attr_reader :name, :matches

    def initialize(options={})
      row = options[:row]

      @name = row[0]
      @matches = []

      parse_row(row)
    end

    private
    def parse_row(row)
      i = 1
      until i >= row.length do
        unless row[i].nil?
          # Format is: index;name;percentage;
          @matches.push Match.new(group: self, index: row[i], name: row[i+1], percentage: row[i+2])
        end

        i += 3
      end
    end
  end

  class Result
    attr_reader :dir

    def initialize(opts = {})
      @dir = opts[:dir]
      csv = CSV.read opts[:file], col_sep: ';'
      @results = []

      csv.each do |row|
        @results.push MatchGroup.new(row: row)
      end
    end

    def each(&block)
      @results.each(&block)
    end
  end

  class Runner
    def self.run(opts = {}, &block)
      Dir.mktmpdir do |tmp|
        system self.jplag_cmd(opts.merge(result: tmp))

        if $?.exitstatus != 0
          raise Error.new("JPlag exited with code: #{$?.exitstatus}")
        else
          yield Result.new(dir: tmp, file: File.join(tmp, "matches_max.csv"))
        end
      end
    end
    JPlag
    private

    def self.jplag_cmd(options = {})
      jplag_opts = ['-s', '-l java19']

      unless options[:basecode].nil?
        jplag_opts.push "-bc #{options[:basecode]}"
      end

      unless options[:result].nil?
        jplag_opts.push "-r #{options[:result]}"
      end

      raise Error.new("Must set directory for submissions!!") if options[:submissions].nil?

      "#{JAVA_BIN} -jar #{JPLAG_BIN} #{jplag_opts.join(' ')} #{options[:submissions]}"
    end
  end
end
