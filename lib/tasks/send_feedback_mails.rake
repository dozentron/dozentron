namespace :dozentron do
  task send_feedback_mails: :environment do
    Feedback.where(mail_enqueued: true).each do |f|
      next unless (f.mail_enqueued_at + 1.hour) <= Time.now

      FeedbackMailer.feedback_mail(f.id).deliver_later
      f.update_attribute :mail_enqueued, false
    end
  end
end
