require 'nokogiri'
require 'net/http'

module THMSearch
  def self.get_user_details(id, endpoint="https://scripts.its.thm.de/emsearch/emsearch.cgi")
    uri = URI(endpoint)
    res = Net::HTTP.post_form(uri, name: id)
    if res.is_a?(Net::HTTPSuccess)
      document = Nokogiri::HTML(res.body)
      lst = document.search('//ul/li').map do |li|
        full_name = li.search('b').first.content.strip
        email = li.search('a').first.content.strip
        found_id = li.search('i').first.content.strip
        name, _, surname = full_name.rpartition(' ')
        return {email: email, id: found_id, display_name: full_name, name: name, surname: surname}
      end
    else
      raise "Querying CGI script didn't work: #{res.message}"
    end
  end
end
