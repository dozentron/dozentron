source 'https://rubygems.org'


# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 5.0'
gem 'railties'
# Use Puma as the app server
gem 'puma'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4.1.0'
# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby

# Use jquery as the JavaScript library
gem 'jquery-rails'
# Turbolinks makes navigating your web application faster. Read more: https://github.com/turbolinks/turbolinks
# gem 'turbolinks', '~> 5.x'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.0'
# Use Redis adapter to run Action Cable in production
# gem 'redis', '~> 3.0'
# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

group :test do
  gem 'rspec-rails', '~> 3.5'
  gem 'rspec_api_documentation'
  gem 'factory_girl'
  gem 'capybara'
  gem 'capybara-webkit'
end

group :test, :development, :server_development do
  # needed to generate faked names in seed entries for db
  gem 'faker'
end

group :development do
  # Access an IRB console on exception pages or by using <%= console %> in views
  gem 'web-console', '~> 3.0'
end

group :development, :server_development do
  gem 'listen', '~> 3.0.5'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
  # gem 'quiet_assets'
  gem 'better_errors' #nicer errors
  gem 'binding_of_caller'
  gem 'sqlite3' #sqlite in development

  gem 'capistrano', require: false
  gem 'capistrano-rvm', require: false
  gem 'capistrano-bundler', require: false
  gem 'capistrano-rails', require: false
  gem 'capistrano-systemd', require: false
  # gem 'rake', require: false
  gem 'capistrano-maintenance', require: false
  gem 'capistrano-pending', require: false

  gem 'capistrano3-delayed-job', '~> 1.0', require: false

  gem 'slackistrano'
end

group :production, :server_test, :server_development do
  gem 'pg', '~> 0.18' #mysql in production
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]

gem 'carrierwave'

# Authentication
gem 'devise'
gem 'devise_cas_authenticatable'

# Pagination
gem 'will_paginate-bootstrap'

gem 'delayed_job_active_record'

gem 'moss_ruby', git: 'https://HappyKadaver@bitbucket.org/macite/moss-ruby.git', branch: 'master'

gem 'java_submission_tester', git: 'https://git.thm.de/dalt40/java-submission-tester.git', branch: 'master'
# gem 'java_submission_tester', path: '../java-submission-tester'
gem 'python_submission_tester', git: 'https://git.thm.de/dalt40/python_submission_test_runner.git', branch: 'master'
#gem 'python_submission_tester', path: '../python_submission_test_runner'

gem 'nokogiri'

gem 'slim'
gem 'slim-rails'
gem 'therubyracer'
gem 'bootstrap-sass'
gem 'font-awesome-rails'
gem 'bootstrap_form'

gem 'chartkick'

gem 'rails-i18n', '~> 5.0.0.beta1' # For 5.0.0.beta1

gem 'cancancan', '~> 1.10'

gem 'ancestry'

# Repo with pull request to make fuzzily work under Rails 5 Remove if fuzzily ever updates
# gem 'fuzzily', git: 'https://github.com/cardillomarcelo/fuzzily.git', ref: 'bd31bfffa2c40c329bb67265c6f3f93da0c94634'
# this repo is now officially offline quick fix in /config/initializers/fuzzily.rb
gem 'fuzzily', git: 'https://github.com/mezis/fuzzily'
gem 'jquery-ui-rails', '5.0.5'
gem 'rails-jquery-autocomplete'

# rendering markdown
gem 'redcarpet'
# syntax highlighting through rouge
gem 'rouge'
gem 'rouge-rails'
# rendering asciidoc
gem 'asciidoctor'
gem 'asciidoctor-rouge'

gem 'rubyzip'
gem 'mimemagic'

gem 'will_paginate'
gem 'inherited_resources', github: 'activeadmin/inherited_resources'
gem 'activeadmin', '~> 1.1.0'

gem 'daemons'

gem 'exception_notification'

gem 'groupdate'

gem 'whenever'
